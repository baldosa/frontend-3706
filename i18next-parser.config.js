const i18nParser = {
  defaultNamespace: 'translation',
  keepRemoved: true,
  lexers: {
    tsx: ['JsxLexer'],
    ts: ['JavascriptLexer'],
    default: ['JavascriptLexer'],
  },
  locales: ['en', 'es', 'pt_BR', 'fr', 'base'],
  output: 'public/locales/$LOCALE/$NAMESPACE.json',
  input: ['src/**/*.{ts,tsx}'],
};

export default i18nParser;
