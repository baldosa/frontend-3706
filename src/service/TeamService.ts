import { buildAuthToken, client } from '../api/Client.ts';
import { Client, Paths } from '../api/utilities/Definitions';
import { OperationResponse } from 'openapi-client-axios';

export class TeamService {
  private static client: Client = client;

  public static async getTeamMessages(
    userToken: string,
    teamToken: string,
    lastKnownMessage?: string,
    limit?: number,
    lookIntoFuture?: boolean,
  ): OperationResponse<Paths.ConversationsMessagesRetrieve.Responses.$200> {
    return await this.client.conversations_messages_retrieve(
      {
        conversation_token: teamToken,
        last_known_message: lastKnownMessage,
        look_into_future: lookIntoFuture,
        limit: limit,
      },
      null,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    );
  }

  public static async getTeamDetails(
    userToken: string,
    teamId: number,
  ): OperationResponse<Paths.TeamsRetrieve.Responses.$200> {
    return await this.client.teams_retrieve(
      {
        id: teamId,
      },
      null,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    );
  }

  public static async getTeamFiles(
    userToken: string,
    teamToken: string,
    descending?: boolean,
    limit?: number,
  ): OperationResponse<Paths.ConversationsFilesRetrieve.Responses.$200> {
    return await this.client.conversations_files_retrieve(
      {
        conversation_token: teamToken,
        descending: descending,
        limit: limit,
      },
      null,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    );
  }

  public static async sendMessage(
    userToken: string,
    teamToken: string,
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
    content: any,
  ): OperationResponse<Paths.ConversationsMessagesCreate.Responses.$200> {
    return await this.client.conversations_messages_create(
      {
        conversation_token: teamToken,
      },
      content,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    );
  }

  public static async uploadFile(
    userToken: string,
    teamToken: string,
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
    content: any,
  ): OperationResponse<Paths.ConversationsFilesCreate.Responses.$200> {
    return await this.client.conversations_files_create(
      {
        conversation_token: teamToken,
      },
      content,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    );
  }

  public static async getTeams(
    userToken: string,
    skipPersonalWorkspace?: boolean,
    last_message?: boolean,
  ): OperationResponse<Paths.TeamsList.Responses.$200> {
    return await this.client.teams_list(
      {
        skip_personal_workspace: skipPersonalWorkspace,
        last_message: last_message,
      },
      null,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    );
  }

  public static async shareUpload(
    userToken: string,
    filename: string,
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
    content: any,
  ): OperationResponse<Paths.SharesUploadCreate.Responses.$200> {
    content.append('filename', filename);
    return await this.client.shares_upload_create(
      {
        filename,
      },
      content,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    );
  }

  public static async shareProject(
    userToken: string,
    filename: string,
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
    content: any,
  ): OperationResponse<Paths.ExportProject.Responses.$200> {
    content.append('filename', filename);
    return await this.client.export_project(
      {
        filename,
      },
      content,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    );
  }

  public static async listProjects(
    userToken: string
  ): OperationResponse<Paths.ListProjects.Responses.$200> {
    return await this.client.list_projects(
      {}, null,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    );
  }

  public static async importProject(
    userToken: string,
    projectId: number
  ): OperationResponse<Paths.ImportProject.Responses.$200> {
    return await this.client.import_project(
      {
        project_id: projectId
      },
      null,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    )
  }

  public static async downloadFile(
    userToken: string,
    fileId: number,
  ): OperationResponse<Paths.DownloadsRetrieve.Responses.$200> {
    return await this.client.downloads_retrieve({ file_id: fileId }, null, {
      headers: { Authorization: buildAuthToken(userToken) },
    });
  }

  public static async deleteFile(
    userToken: string,
    fileId: number,
  ): OperationResponse<Paths.FilesDestroy.Responses.$204> {
    return await this.client.files_destroy({ file_id: fileId }, null, {
      headers: { Authorization: buildAuthToken(userToken) },
    });
  }

  public static async getContentTypes(
    userToken: string,
  ): OperationResponse<Paths.FilesContentTypesRetrieve.Responses.$200> {
    return await this.client.files_content_types_retrieve(null, null, {
      headers: { Authorization: buildAuthToken(userToken) },
    });
  }
}
