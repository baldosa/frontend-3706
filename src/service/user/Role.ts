export enum Role {
  Collaborator = 'Collaborator',
  Admin = 'Admin',
  OrgOwner = 'OrgOwner',
}

export function getAllRoles(): Role[] {
  return [Role.Collaborator, Role.Admin, Role.OrgOwner];
}
