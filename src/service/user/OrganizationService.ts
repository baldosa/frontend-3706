import { buildAuthToken, client } from '../../api/Client.ts';
import { Client, Paths } from '../../api/utilities/Definitions';
import { OperationResponse } from 'openapi-client-axios';

export class OrganizationService {
  private static client: Client = client;

  public static async getOrganization(
    organizationId: number,
    userToken: string,
  ): OperationResponse<Paths.OrganizationsRetrieve.Responses.$200> {
    return await this.client.organizations_retrieve(
      { id: organizationId },
      null,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    );
  }

  public static async getOrganizationsByUser(
    userToken: string,
  ): OperationResponse<Paths.OrganizationsByuserRetrieve.Responses.$200> {
    return await this.client.organizations_byuser_retrieve(null, null, {
      headers: { Authorization: buildAuthToken(userToken) },
    });
  }

  public static async getMembersOrganizations(
    organizationId: number,
    userToken: string,
  ): OperationResponse<Paths.GetOrganizationMembers.Responses.$200> {
    return await this.client.get_organization_members(
      { organization_id: organizationId },
      null,
      { headers: { Authorization: buildAuthToken(userToken) } },
    );
  }

  public static async updateOrganization(
    organizationId: number,
    userToken: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data: any,
  ): OperationResponse<Paths.OrganizationsRetrieve.Responses.$200> {
    return await this.client.organizations_partial_update(
      { id: organizationId },
      data,
      {
        headers: { Authorization: buildAuthToken(userToken) },
      },
    );
  }

  public static async getInvitations(
    organizationId: number,
    userToken: string,
  ): OperationResponse<Paths.ListInvitations.Responses.$200> {
    return await this.client.list_invitations(
      { organization_id: organizationId },
      null,
      { headers: { Authorization: buildAuthToken(userToken) } },
    );
  }
}
