import { buildAuthToken, client } from '../../api/Client.ts';
import { Client, Components, Paths } from '../../api/utilities/Definitions';
import { OperationResponse } from 'openapi-client-axios';
import { AxiosResponse } from 'axios';

export class AuthService {
  private static client: Client = client;

  public static async login(
    username: string,
    password: string,
  ): OperationResponse<Components.Schemas.JWT> {
    const params = username.includes('@')
      ? { email: username, password: password }
      : { username: username, password: password };
    const userLoginResponse = await this.client
      .auth_login_create(null, params)
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      .then(async (userLoginResponse: AxiosResponse<any>) => {
        userLoginResponse.data.user.organizationId = await this.client
          .organizations_byuser_retrieve(null, null, {
            headers: {
              Authorization: buildAuthToken(userLoginResponse.data.access),
            },
          })
          .then((response) => {
            return response.data.id;
          })
          .catch(() => {
            return null;
          });
        return userLoginResponse;
      });

    if (userLoginResponse.data) {
      userLoginResponse.data.user.roles = [
        userLoginResponse.data.user.group.name,
      ];
      localStorage.setItem('user', JSON.stringify(userLoginResponse.data));
    }

    return userLoginResponse;
  }

  public static async signup(
    parameters: Paths.ConfirmInvitation.PathParameters,
    username: string,
    password: string,
  ): OperationResponse<Components.Schemas.ConfirmInvitation> {
    return await this.client.confirm_invitation(parameters, {
      username: username,
      password: password,
    });
  }

  public static async logout(token: string) {
    return await this.client.auth_logout_create(null, null, {
      headers: { Authorization: buildAuthToken(token) },
    });
  }

  public static async passwordReset(email: string) {
    return await this.client.auth_password_reset_create(null, { email });
  }

  public static async passwordResetConfirm(
    uid: string,
    token: string,
    newPassword: string,
    newPasswordConfirm: string,
  ) {
    return await this.client.auth_password_reset_confirm_create(null, {
      uid: uid,
      password: newPassword,
      password_confirm: newPasswordConfirm,
      token,
    });
  }

  public static async refreshToken(
    refreshToken: string,
  ): Promise<AxiosResponse<Paths.AuthTokenRefreshCreate.Responses.$200>> {
    return await this.client.auth_token_refresh_create(null, {
      refresh: refreshToken,
    });
  }

  public static async verifyToken(
    token: string,
  ): Promise<AxiosResponse<Components.Schemas.TokenVerify>> {
    return await this.client.auth_token_verify_create(null, { token: token });
  }
}
