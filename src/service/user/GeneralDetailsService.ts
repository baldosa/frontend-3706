import { buildAuthToken, client } from '../../api/Client.ts';
import { Client, Paths } from '../../api/utilities/Definitions';
import { OperationResponse } from 'openapi-client-axios';

export class GeneralDetailsService {
  private static client: Client = client;

  public static async getLanguages(
    sessionToken: string,
  ): OperationResponse<Paths.LanguagesList.Responses.$200> {
    return await this.client.languages_list(null, null, {
      headers: { Authorization: buildAuthToken(sessionToken) },
    });
  }

  public static async getCountries(
    sessionToken: string,
  ): OperationResponse<Paths.CountriesList.Responses.$200> {
    return await this.client.countries_list(null, null, {
      headers: { Authorization: buildAuthToken(sessionToken) },
    });
  }

  public static async sendInvitations(
    organization_id: number,
    sessionToken: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    invitations: any,
  ): OperationResponse<Paths.InvitationsOrganizationmemberCreate.Responses.$200> {
    return await this.client.invitations_organizationmember_create(
      null,
      {
        organization_id: organization_id,
        new_users: invitations,
      },
      { headers: { Authorization: buildAuthToken(sessionToken) } },
    );
  }

  public static async getGroups(
    sessionToken: string,
  ): OperationResponse<Paths.GroupsList.Responses.$200> {
    return await this.client.groups_list(null, null, {
      headers: { Authorization: buildAuthToken(sessionToken) },
    });
  }

  public static async getOrganization(
    sessionToken: string,
  ): OperationResponse<Paths.GroupsList.Responses.$200> {
    return await this.client.groups_list(null, null, {
      headers: { Authorization: buildAuthToken(sessionToken) },
    });
  }
}
