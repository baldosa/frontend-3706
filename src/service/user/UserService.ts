import { buildAuthToken, client } from '../../api/Client.ts';
import { Client, Paths } from '../../api/utilities/Definitions';
import { OperationResponse } from 'openapi-client-axios';

export class UserService {
  private static client: Client = client;

  public static async getUserData(
    userId: number,
    userToken: string,
  ): OperationResponse<Paths.UsersRetrieve.Responses.$200> {
    return await this.client.users_retrieve({ id: userId }, null, {
      headers: { Authorization: buildAuthToken(userToken) },
    });
  }

  public static async updateUserData(
    userId: number,
    userToken: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    data: any,
  ): OperationResponse<Paths.UsersPartialUpdate.Responses.$200> {
    return await this.client.users_partial_update({ id: userId }, data, {
      headers: { Authorization: buildAuthToken(userToken) },
    });
  }

  public static async getUserList(
    userToken: string,
  ): OperationResponse<Paths.UsersList.Responses.$200> {
    return await this.client.users_list(undefined, undefined, {
      headers: { Authorization: buildAuthToken(userToken) },
    });
  }
}
