import { buildAuthToken, client } from '../../api/Client.ts';
import { Client, Paths } from '../../api/utilities/Definitions';
import { OperationResponse } from 'openapi-client-axios';

export class GroupService {
  private static client: Client = client;

  public static async getGroups(
    userToken: string,
  ): OperationResponse<Paths.GroupsList.Responses.$200> {
    return await this.client.groups_list(null, null, {
      headers: { Authorization: buildAuthToken(userToken) },
    });
  }

  public static async getGroupData(
    userToken: string,
    groupId: number,
  ): OperationResponse<Paths.GroupsRetrieve.Responses.$200> {
    return await this.client.groups_retrieve({ id: groupId }, null, {
      headers: { Authorization: buildAuthToken(userToken) },
    });
  }
}
