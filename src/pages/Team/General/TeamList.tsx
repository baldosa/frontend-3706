import * as React from 'react';
import { Dispatch, useEffect, useState } from 'react';
import {
  ListItem,
  Menu,
  MenuContent,
  PageSection,
  Title,
} from '@patternfly/react-core';
import { TeamItem } from './TeamItem.tsx';
import { TeamService } from '../../../service/TeamService.ts';
import { useDispatch, useSelector } from 'react-redux';
import { getAccessToken } from '../../../redux/reducers/auth.ts';
import { useTranslation } from 'react-i18next';
import {
  setHeaderDescription,
  setHeaderName,
  setHeaderNavigationDestination,
} from '../../../redux/actions/generalActions.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { useParams } from 'react-router-dom';

export const TeamList: React.FunctionComponent = () => {
  const [getTeams, setTeams] = useState<React.ReactElement[]>([]);
  const getTokenFromUser = useSelector(getAccessToken);
  const { organizationId } = useParams();
  const { t } = useTranslation();
  const dispatch: Dispatch<AnyAction> = useDispatch();

  useEffect(() => {
    setTeams(
      [...Array(3)].map(
        (_, index) => (
          <TeamItem
            id={`team-list-item-${index}`}
            key={`team-list-item-${index}`}
            isLoading={true}
          />
        ),
        [],
      ),
    );
  }, []);

  useEffect((): void => {
    setHeaderName(t('structure_menu.teams.list.header').toString())(dispatch);
    setHeaderDescription(t('structure_menu.teams.list.description').toString())(
      dispatch,
    );
    setHeaderNavigationDestination('')(dispatch);
    TeamService.getTeams(getTokenFromUser, true, true)
      .then((response): void => {
        setTeams(
          response.data.map((team, index) => (
            <TeamItem
              id={`team-list-item-${index}`}
              key={`team-list-item-${index}`}
              /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
              organizationId={parseInt(organizationId!)}
              teamId={team.id}
              name={team.name || team.organization_name}
              lastMessageTimestamp={team.last_message?.last_message_timestamp}
              amountFiles={0}
              isLoading={false}
            />
          )),
        );
      })
      .catch((): void => {
        setTeams([
          <ListItem
            key={'`team-list-item-error'}
            className={'list-item'}
            style={{ justifyContent: 'space-around' }}
            children={
              <Title
                style={{ whiteSpace: 'pre-line' }}
                headingLevel={'h1'}
                children={t('structure_menu.teams.preview_error')}
              />
            }
          />,
        ]);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <PageSection>
      <Menu isScrollable isPlain={true} className={'structure-menu'}>
        <MenuContent>{getTeams}</MenuContent>
      </Menu>
    </PageSection>
  );
};
