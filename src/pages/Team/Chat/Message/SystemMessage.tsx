import * as React from 'react';
import { MessageParameters } from './Message.tsx';
import { Divider, Title, Tooltip } from '@patternfly/react-core';
import { parseMessage } from '../../../../utils/teamUtils.ts';
import InfoCircleIcon from '@patternfly/react-icons/dist/esm/icons/info-circle-icon';

export const SystemMessage: React.FunctionComponent<{
  id: number;
  fullDate: string;
  simplifiedDateTime: string;
  message: string;
  messageParameters?: MessageParameters;
}> = (props): React.JSX.Element => {
  return (
    <div
      id={`message-container-${props.id}`}
      key={`message-container-${props.id}`}
      className={'system-message'}
    >
      <Tooltip aria="none" aria-live="polite" content={props.fullDate}>
        <Title
          headingLevel={'h6'}
          children={props.simplifiedDateTime}
          id={'message-hour'}
        />
      </Tooltip>
      <div className={'system-message-structure'}>
        <Divider component="li" />
        <div className={'system-message-structure-container'}>
          <InfoCircleIcon
            style={{
              marginRight: '5px',
              height: '24px',
              width: '24px',
              color: '#73c5c5',
            }}
          />
          <Title
            className={'system-message-content'}
            headingLevel={'h6'}
            children={parseMessage(props.message, props.messageParameters)}
          />
        </div>
        <Divider component="li" />
      </div>
    </div>
  );
};
