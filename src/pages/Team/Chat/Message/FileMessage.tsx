import * as React from 'react';
import {
  Button,
  Card,
  CardBody,
  Dropdown,
  KebabToggle,
  Modal,
} from '@patternfly/react-core';
import { onDownload } from '../../../../utils/FileUtils.tsx';
import { useDispatch, useSelector } from 'react-redux';
import {
  getAccessToken,
  getUsername,
} from '../../../../redux/reducers/auth.ts';
import { useTranslation } from 'react-i18next';
import {
  completeActionForContent,
  initiateActionForContent,
} from '../../../../redux/actions/teams.ts';
import { Dispatch, Fragment, ReactElement, useState } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import { contentsActionInProgress } from '../../../../redux/reducers/teams.ts';
import { RootState } from '../../../../redux/store.ts';
import {
  createPreview,
  isFilePreviewAvailable,
  onDelete,
  renderFullName,
} from '../../../../utils/logics/fileUtils.ts';
import { MessageContent } from './MessageContent.tsx';
import { addAlert } from '../../../../redux/actions/alert.ts';

export const FileMessage: React.FunctionComponent<{
  id: number;
  fileId: number;
  teamOrigin: string;
  ownerId: string;
  name: string;
  extension: string;
  delete: (messageId: number) => void;
}> = (props): React.JSX.Element => {
  const [getContentPreviewStatus, setContentPreviewStatus] =
    useState<boolean>(false);
  const [getStatusOfContentDownload, setStatusOfContentDownload] =
    useState<boolean>(false);
  const [getStatusOfContentDeletion, setStatusOfContentDeletion] =
    useState<boolean>(false);

  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const [getModalContent, setModalContent] = useState<ReactElement | undefined>(
    undefined,
  );
  const [getModalActions, setModalActions] = useState<ReactElement[]>([]);
  const [getModalTitle, setModalTitle] = useState<string>('');
  const [getModalClass, setModalClass] = useState<string>('preview');

  const [isOpen, setIsOpen] = React.useState<boolean>(false);

  const userToken = useSelector(getAccessToken);
  const username = useSelector(getUsername);

  const { t } = useTranslation();
  const dispatch: Dispatch<AnyAction> = useDispatch();

  const listOfContentActionInProgress = useSelector((state: RootState) =>
    contentsActionInProgress(state, props.teamOrigin),
  );

  const thereIsAContentActionInProgress = (contentId: number) => {
    return listOfContentActionInProgress.includes(contentId);
  };

  const onToggle = (isOpen: boolean): void => {
    setIsOpen(isOpen);
  };

  const isOwnerMessage = () => {
    return props.ownerId === username;
  };

  const openModal = () => {
    setIsModalOpen(true);
  };

  const closeModal = () => {
    setIsModalOpen(false);
  };

  const downloadContent = () => {
    initiateActionForContent(props.teamOrigin, props.fileId)(dispatch);
    setStatusOfContentDownload(true);
    onDownload(
      props.fileId,
      userToken,
      /* eslint-disable-next-line @typescript-eslint/no-non-null-assertion */
      renderFullName(props.name!, props.extension!),
    )
      .catch((error) => {
        console.error(error);
      })
      .finally(() => {
        setStatusOfContentDownload(false);
        completeActionForContent(props.teamOrigin, props.fileId)(dispatch);
      });
  };

  const deleteContent = () => {
    initiateActionForContent(props.teamOrigin, props.fileId)(dispatch);
    setStatusOfContentDeletion(true);
    onDelete(props.fileId, userToken)
      .then(() => {
        props.delete(props.id);
        addAlert(
          'success',
          t('structure_menu.teams.files.delete.alert.success', {
            name: props.name,
          }),
        )(dispatch);
      })
      .catch(() => {
        closeModal();
        addAlert(
          'danger',
          t('structure_menu.teams.files.delete.alert.error', {
            name: props.name,
          }),
        )(dispatch);
      })
      .finally(() => {
        setStatusOfContentDeletion(false);
        completeActionForContent(props.teamOrigin, props.fileId)(dispatch);
      });
  };

  const loadPreviewContent = () => {
    initiateActionForContent(props.teamOrigin, props.fileId)(dispatch);
    setContentPreviewStatus(true);
    createPreview(
      props.fileId,
      userToken,
      props.extension,
      t('structure_menu.teams.files.preview.file_aria_label', {
        name: props.name,
      }),
    )
      .then((result: ReactElement) => {
        openModal();
        setModalContent(result);
      })
      .catch(() => {
        closeModal();
      })
      .finally(() => {
        setContentPreviewStatus(false);
        completeActionForContent(props.teamOrigin, props.fileId)(dispatch);
      });
  };

  const loadDataForModalPreview = () => {
    setModalClass('preview');
    setModalTitle('');
    setModalActions([]);
    setModalContent(undefined);
    loadPreviewContent();
  };

  const prepareRemoveFileModal = () => {
    setModalClass('remove');
    setModalTitle(
      t('structure_menu.teams.files.delete.modal.title', {
        name: props.name,
      }).toString(),
    );
    setModalActions([
      <Button
        key="confirm"
        variant="primary"
        children={t(
          'structure_menu.teams.files.delete.modal.confirm',
        ).toString()}
        onClick={deleteContent}
      />,
      <Button
        key="cancel"
        variant="link"
        children={t(
          'structure_menu.teams.files.delete.modal.cancel',
        ).toString()}
        onClick={closeModal}
      />,
    ]);
    setModalContent(undefined);
    openModal();
  };

  return (
    <Fragment>
      <Card
        id={`message-container-${props.id}`}
        key={`message-container-${props.id}`}
      >
        <CardBody
          className={'body file-message-body'}
          style={{ padding: '10px' }}
        >
          <Dropdown
            style={{ minWidth: 'min-content', float: 'right' }}
            position={'right'}
            menuAppendTo={'parent'}
            isOpen={isOpen}
            direction={'up'}
            isPlain
            toggle={<KebabToggle id="toggle-kebab" onToggle={onToggle} />}
            dropdownItems={[
              <Button
                key={`download-file-${props.fileId}`}
                className={'message_actions_button'}
                isLoading={
                  getStatusOfContentDownload &&
                  thereIsAContentActionInProgress(props.fileId)
                }
                isDisabled={thereIsAContentActionInProgress(props.fileId)}
                onClick={downloadContent}
                children={t('structure_menu.teams.files.download')}
              />,
              <Button
                key="delete-file"
                className={'message_actions_button'}
                isLoading={
                  getStatusOfContentDeletion &&
                  thereIsAContentActionInProgress(props.fileId)
                }
                isDisabled={thereIsAContentActionInProgress(props.fileId)}
                style={{ display: isOwnerMessage() ? 'block' : 'none' }}
                onClick={prepareRemoveFileModal}
                children={t('structure_menu.teams.files.delete.button')}
              />,
              <Button
                key="preview-file"
                className={'message_actions_button'}
                isLoading={
                  getContentPreviewStatus &&
                  thereIsAContentActionInProgress(props.fileId)
                }
                isDisabled={thereIsAContentActionInProgress(props.fileId)}
                style={{
                  display: isFilePreviewAvailable(props.extension)
                    ? 'block'
                    : 'none',
                }}
                onClick={loadDataForModalPreview}
                children={t('structure_menu.teams.files.preview.button')}
              />,
            ]}
          />
          <MessageContent
            fileId={props.fileId}
            name={props.name}
            extension={props.extension}
          />
        </CardBody>
      </Card>
      <Modal
        className={`file-action-modal ${getModalClass}`}
        variant={'default'}
        title={getModalTitle}
        actions={getModalActions}
        isOpen={isModalOpen}
        onClose={closeModal}
        children={getModalContent}
        aria-label={t(
          'structure_menu.teams.files.delete.modal.aria_label',
        ).toString()}
      />
    </Fragment>
  );
};
