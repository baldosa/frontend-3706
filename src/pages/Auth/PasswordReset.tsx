import React, { Dispatch, useState } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Form,
  Modal,
  ModalBoxBody,
  ModalVariant,
  PageSection,
  TextContent,
} from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { TopSection } from '../../components/Auth/TopSection.js';
import { ExtendedTextInput } from '../../components/ExtendedTextInput.js';
import {
  EMAIL,
  GENERAL,
  isAnInvalidFormat,
  isEmpty,
  PROFILE,
} from '../../components/Auth/ValidationRules.ts';
import { passwordReset } from '../../redux/actions/auth.js';
import { useNavigate } from 'react-router-dom';
import {
  defineValidationRequestLocation,
  endAction,
  initiateAction,
} from '../../redux/actions/generalActions.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { useDispatch, useSelector } from 'react-redux';
import {
  allBlockingRequestsWereValidated,
  getActionState,
} from '../../redux/reducers/generalActions.ts';
import { ExtendedHelperText } from '../../components/ExtendedHelperText.tsx';
import { loginFullPath } from '../../routes.tsx';

export const PasswordReset: React.FunctionComponent = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const [userEmail, setUserEmail] = useState<string>('');
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [showHelpInformation, setShowHelpInformation] = React.useState(false);
  const isActionInProgress = useSelector(getActionState);
  const isValid = useSelector(allBlockingRequestsWereValidated);
  const dispatch: Dispatch<AnyAction> = useDispatch();

  const handleResetPasswordButton = () => {
    initiateAction()(dispatch);
    passwordReset(userEmail)
      .then(() => handleModalToggle())
      .catch((error) => {
        defineValidationRequestLocation(error)(dispatch);
      })
      .finally(() => endAction()(dispatch));
  };

  const handleBackToHomeButton = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
  ): void => {
    event.preventDefault();
    navigate('/');
  };

  const handleHelpInformationToggle = () => {
    setShowHelpInformation(!showHelpInformation);
  };

  const handleResendPasswordResetEmail = () => {
    passwordReset(userEmail);
  };

  const handleModalToggle = () => {
    setIsModalOpen(!isModalOpen);
  };

  return (
    <PageSection isCenterAligned style={{ height: '90%' }}>
      <Card className={'auth-page-card'} isPlain>
        <CardTitle>
          <TopSection title={t('password_reset.form.title').toString()} />
        </CardTitle>
        <CardBody>
          <TextContent
            className={'align-left-text'}
            style={{ paddingBottom: '1rem' }}
          >
            {t('password_reset.form.upper_section')}
          </TextContent>
          <TextContent
            className={'align-left-text'}
            style={{ paddingBottom: '1rem' }}
          >
            {t('password_reset.form.middle_section')}
          </TextContent>

          <Form id={'password_reset_form'} style={{ paddingTop: '1rem' }}>
            <ExtendedTextInput
              id={'password_reset_email_text_input'}
              origin={PROFILE}
              fieldNames={[EMAIL]}
              label={t('password_reset.form.email_label').toString()}
              isRequired={true}
              type={'email'}
              getValue={userEmail}
              setValue={setUserEmail}
              placeholder={t(
                'password_reset.form.email_placeholder',
              ).toString()}
              helperText={''}
              rules={[isEmpty, isAnInvalidFormat]}
            />
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                paddingTop: '1rem',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Button
                isDisabled={!isValid}
                className={'submitButton'}
                onClick={handleResetPasswordButton}
                isLoading={isActionInProgress}
                spinnerAriaValueText={t('general.awaiting_response').toString()}
              >
                {t('password_reset.form.button_content')}
              </Button>
              <div style={{ marginTop: '25px' }}>
                {t('password_reset.form.back_to_login')}
                <Button
                  variant={'link'}
                  style={{ padding: 0, color: '#8e93b7', marginLeft: '2px' }}
                  children={t('password_reset.form.login')}
                  onClick={() => navigate(loginFullPath())}
                />
              </div>
            </div>

            <Modal
              aria-label={'post_password_reset_modal'}
              variant={ModalVariant.small}
              title={t('password_reset.modal.header').toString()}
              isOpen={isModalOpen}
              onClose={handleModalToggle}
              showClose={false}
            >
              <ModalBoxBody className={'post_password_reset_modal_body'}>
                <TextContent
                  className={'align-left-text'}
                  style={{ paddingBottom: '1rem' }}
                >
                  {t('password_reset.modal.text_content', {
                    email_address: userEmail,
                  })}
                </TextContent>
                <a
                  className={'bottomSectionElement'}
                  onClick={() => handleHelpInformationToggle()}
                >
                  {t('password_reset.modal.need_help')}
                </a>
                {showHelpInformation && (
                  <div>
                    <TextContent
                      className={'align-left-text'}
                      style={{ paddingBottom: '1rem' }}
                    >
                      {t('password_reset.modal.help_information.spam')}
                    </TextContent>
                    <TextContent
                      className={'align-left-text'}
                      style={{ paddingBottom: '1rem' }}
                    >
                      {t('password_reset.modal.help_information.issue')}
                    </TextContent>
                    <TextContent
                      className={'align-left-text'}
                      style={{ paddingBottom: '1rem' }}
                    >
                      {t('password_reset.modal.help_information.resend')}
                      <a
                        className={'bottomSectionElement'}
                        onClick={() => handleResendPasswordResetEmail()}
                      >
                        {' '}
                        {t('password_reset.modal.help_information.click_here')}
                      </a>
                    </TextContent>
                  </div>
                )}
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    paddingTop: '2rem',
                  }}
                >
                  <Button
                    className={'submitButton'}
                    onClick={handleBackToHomeButton}
                  >
                    {t('password_reset.modal.button_content')}
                  </Button>
                </div>
              </ModalBoxBody>
            </Modal>
          </Form>
          <ExtendedHelperText
            className={'bottomSection'}
            origin={GENERAL}
            fieldNames={[GENERAL]}
            validationRules={[]}
          />
        </CardBody>
      </Card>
    </PageSection>
  );
};
