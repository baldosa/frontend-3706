import React, { Dispatch, useState } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Form,
  Modal,
  ModalBoxBody,
  ModalVariant,
  PageSection,
  TextContent,
} from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { TopSection } from '../../components/Auth/TopSection.js';
import { PasswordInput } from '../../components/PasswordInput.js';
import {
  PASSWORD,
  isAnInvalidExtension,
  isAnInvalidFormat,
  isEmpty,
  SIGNUP,
  GENERAL,
  PASSWORD_CONFIRM,
  INTERNAL,
  PASSWORD_RESET,
} from '../../components/Auth/ValidationRules.ts';
import { passwordResetConfirm } from '../../redux/actions/auth.js';
import { Params, useNavigate, useParams } from 'react-router-dom';
import { loginFullPath } from '../../routes.js';
import { useDispatch, useSelector } from 'react-redux';
import {
  allBlockingRequestsWereValidated,
  getActionState,
} from '../../redux/reducers/generalActions.ts';
import { AnyAction } from '@reduxjs/toolkit';
import {
  defineValidationRequestLocation,
  endAction,
  initiateAction,
  updateNonBlockingValidationRequest,
} from '../../redux/actions/generalActions.ts';
import { ExtendedHelperText } from '../../components/ExtendedHelperText.tsx';

export const PasswordResetConfirm: React.FunctionComponent = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const parameters: Readonly<Params> = useParams();
  const [newPassword, setNewPassword] = useState<string>('');
  const [newPasswordConfirmation, setNewPasswordConfirmation] =
    useState<string>('');
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const isActionInProgress = useSelector(getActionState);
  const isValid = useSelector(allBlockingRequestsWereValidated);

  const dispatch: Dispatch<AnyAction> = useDispatch();

  const handlePasswordResetConfirmButton = () => {
    if (newPassword === newPasswordConfirmation) {
      initiateAction()(dispatch);
      passwordResetConfirm(
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        parameters.token!,
        newPassword,
        newPasswordConfirmation,
      )
        .then(() => handleModalToggle())
        .catch((error) => {
          defineValidationRequestLocation(error)(dispatch);
        })
        .finally(() => endAction()(dispatch));
    } else {
      updateNonBlockingValidationRequest(
        [GENERAL],
        INTERNAL,
        `ERROR_${PASSWORD_RESET}_${PASSWORD}_MISMATCH`,
        'error',
        undefined,
      )(dispatch);
    }
  };

  const handleGoToLoginButton = () => {
    navigate(loginFullPath());
  };

  const handleModalToggle = () => {
    setIsModalOpen(!isModalOpen);
  };

  return (
    <PageSection isCenterAligned style={{ height: '90%' }}>
      <Card className={'auth-page-card'} isPlain>
        <CardTitle>
          <TopSection
            title={t('password_reset_confirm.form.title').toString()}
          />
        </CardTitle>
        <CardBody>
          <TextContent
            className={'align-left-text'}
            style={{ paddingBottom: '1rem' }}
          >
            {t('password_reset_confirm.form.upper_section')}
          </TextContent>
          <Form
            id={'password_reset_confirm_form'}
            style={{ paddingTop: '1rem' }}
          >
            <PasswordInput
              id={'password_reset_text_input'}
              className="password-input"
              origin={SIGNUP}
              fieldNames={[PASSWORD]}
              label={t('password_reset_confirm.form.password_label').toString()}
              isRequired={true}
              type={'password'}
              getValue={newPassword}
              setValue={setNewPassword}
              placeholder={t(
                'password_reset_confirm.form.password_placeholder',
              ).toString()}
              helperText={''}
              rules={[isEmpty, isAnInvalidFormat, isAnInvalidExtension]}
            />
            <PasswordInput
              id={'password_reset_confirm_text_input'}
              className="password-input"
              origin={SIGNUP}
              fieldNames={[PASSWORD_CONFIRM]}
              label={t('password_reset_confirm.form.password_label').toString()}
              isRequired={true}
              type={'password'}
              getValue={newPasswordConfirmation}
              setValue={setNewPasswordConfirmation}
              placeholder={t(
                'password_reset_confirm.form.password_confirm_placeholder',
              ).toString()}
              helperText={''}
              rules={[isEmpty, isAnInvalidFormat, isAnInvalidExtension]}
            />
            <div
              style={{
                display: 'flex',
                justifyContent: 'end',
                paddingRight: '3rem',
                paddingTop: '1rem',
              }}
            >
              <Button
                isDisabled={!isValid}
                className={'submitButton'}
                onClick={handlePasswordResetConfirmButton}
                isLoading={isActionInProgress}
                spinnerAriaValueText={t('general.awaiting_response').toString()}
              >
                {t('password_reset_confirm.form.button_content')}
              </Button>
            </div>
          </Form>
          <div className={'bottomSection'}>
            <ExtendedHelperText
              origin={GENERAL}
              fieldNames={[GENERAL]}
              validationRules={[]}
            />
          </div>
          <Modal
            aria-label={'post_password_reset_confirm_modal'}
            variant={ModalVariant.small}
            title={t('password_reset_confirm.modal.header').toString()}
            isOpen={isModalOpen}
            onClose={handleModalToggle}
            showClose={false}
          >
            <ModalBoxBody className={'post_password_reset_confirm_modal_body'}>
              <TextContent
                className={'align-left-text'}
                style={{ paddingBottom: '1rem' }}
              >
                {t('password_reset_confirm.modal.text_content')}
              </TextContent>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                  paddingTop: '2rem',
                }}
              >
                <Button
                  className={'submitButton'}
                  onClick={handleGoToLoginButton}
                >
                  {t('password_reset_confirm.modal.button_content')}
                </Button>
              </div>
            </ModalBoxBody>
          </Modal>
        </CardBody>
      </Card>
    </PageSection>
  );
};
