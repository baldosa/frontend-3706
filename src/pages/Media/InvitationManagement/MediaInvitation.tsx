import { FunctionComponent } from 'react';
import { Avatar, Card, CardBody, CardTitle } from '@patternfly/react-core';
import avatar from '../../../assets/pages/User/Configuration/avatar.png';
import { convertBase64ToBlobURL } from '../../../utils/logics/fileUtils.ts';
import { useTranslation } from 'react-i18next';

export const MediaInvitation: FunctionComponent<{
  name: string;
  avatar: string;
  status: string;
  invited_by: string;
  key: string;
}> = (props) => {
  const { t } = useTranslation();

  return (
    <Card className={'media_member'}>
      <Avatar
        id={'message-avatar'}
        className={'avatar-icon'}
        alt={'avatar'}
        size={'md'}
        src={props.avatar ? convertBase64ToBlobURL(props.avatar) : avatar}
        style={{ marginLeft: '15px' }}
      />
      <div>
        <CardTitle style={{ paddingBottom: 0 }} children={props.name} />
        <CardBody
          style={{ paddingBottom: 0 }}
          children={t(
            'media_section.invitations_management.invitation.invited_by',
            {
              name: props.name,
            },
          )}
        />
        <CardBody children={props.status} />
      </div>
    </Card>
  );
};
