import { FunctionComponent, ReactElement, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import {
  getAccessToken,
  getOrganizationId as getOrganizationIdFromState,
} from '../../../redux/reducers/auth.ts';
import { Tab } from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { OrganizationService } from '../../../service/user/OrganizationService.ts';
import { MediaMember } from './MediaMember.tsx';
import { Components } from '../../../api/utilities/Definitions';
import { MemberStatus } from './MemberStatus.ts';
import { MediaEntitiesList } from '../MediaEntitiesList.tsx';

export const MediaMembersList: FunctionComponent = () => {
  const getOrganizationId = useSelector(getOrganizationIdFromState);
  const getTokenFromUser = useSelector(getAccessToken);

  const [getDataStatus, setDataStatus] = useState<boolean>(false);

  const [getActiveMembers, setActiveMembers] = useState<ReactElement[]>([]);
  const [getPendingMembers, setPendingMembers] = useState<ReactElement[]>([]);

  const [getAmountOfActiveMembers, setAmountOfActiveMembers] =
    useState<number>(0);
  const [getAmountOfPendingMembers, setAmountOfPendingMembers] =
    useState<number>(0);

  const [getMembers, setMembers] = useState<ReactElement[]>([]);
  const [getAmountOfMembers, setAmountOfMembers] = useState<number>(0);

  const [activeTabKey, setActiveTabKey] = useState<MemberStatus>(
    MemberStatus.ACTIVE,
  );

  const { t } = useTranslation();

  useEffect(() => {
    setOrgMembers(getOrganizationId, getTokenFromUser);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const intervalToGetNewMembers = setInterval(() => {
      setDataStatus(false);
      setOrgMembers(getOrganizationId, getTokenFromUser);
    }, 10000);

    return (): void => {
      clearInterval(intervalToGetNewMembers);
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (getDataStatus) {
      switch (activeTabKey) {
        case MemberStatus.ACTIVE:
          setMembers(getActiveMembers);
          setAmountOfMembers(getAmountOfActiveMembers);
          break;
        case MemberStatus.SUSPENDED:
          setMembers(getPendingMembers);
          setAmountOfMembers(getAmountOfPendingMembers);
          break;
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getDataStatus, activeTabKey]);

  const setOrgMembers = (organizationId: number, userToken: string) => {
    OrganizationService.getMembersOrganizations(organizationId, userToken)
      .then((response) => {
        const activeMembersData: Components.Schemas.OrganizationMembersData =
          response.data.active;
        const suspendedMembersData: Components.Schemas.OrganizationMembersData =
          response.data.suspended;
        setActiveMembers(renderMediaMembers(activeMembersData.users));
        setPendingMembers(renderMediaMembers(suspendedMembersData.users));
        setAmountOfActiveMembers(activeMembersData.amount);
        setAmountOfPendingMembers(suspendedMembersData.amount);
        setDataStatus(true);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const renderMediaMembers = (membersData: any[]) => {
    return membersData.map((userData, index) => (
      <MediaMember
        key={`media_member_${index}`}
        name={userData.user.full_name}
        group={userData.group.name}
        avatar={userData.user.avatar}
      />
    ));
  };

  const handleTabSelect = (tabKey: string) => {
    setActiveTabKey(tabKey as MemberStatus);
  };

  return (
    <MediaEntitiesList
      entities={getMembers}
      amount={getAmountOfMembers}
      title={t('media_section.members_management.title')}
      activeTab={activeTabKey}
      tabs={[
        <Tab
          eventKey={MemberStatus.ACTIVE}
          title={t('media_section.members_management.tabs.active_members', {
            amount: getAmountOfActiveMembers,
          })}
        />,
        <Tab
          eventKey={MemberStatus.SUSPENDED}
          title={t('media_section.members_management.tabs.suspended_members', {
            amount: getAmountOfPendingMembers,
          })}
        />,
      ]}
      onTabSelect={handleTabSelect}
    />
  );
};
