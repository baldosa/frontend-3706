import React from 'react';
import TimesIcon from '@patternfly/react-icons/dist/esm/icons/times-icon';
import {
  ListItem,
  List,
  ListComponent,
  OrderType,
  BackToTop,
  PageSection,
  Text,
  Button,
  Modal,
  ModalVariant,
  ModalBoxBody,
  ModalBoxFooter,
} from '@patternfly/react-core';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { cancelTAC, confirmTAC } from '../../redux/actions/auth.ts';
import { Trans, useTranslation } from 'react-i18next';
import { loginFullPath } from '../../routes.tsx';

export const TermsAndConditions: React.FunctionComponent<{
  isProtected?: boolean;
}> = ({ isProtected = false }) => {
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [isAlwaysVisible] = React.useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const acceptTermsAndConditions = (): void => {
    confirmTAC()(dispatch);
    navigate(-1);
  };

  const backToLogin = (): void => {
    cancelTAC()(dispatch);
    navigate(loginFullPath());
  };

  const handleModalToggle = () => {
    setIsModalOpen(!isModalOpen);
  };

  return (
    <PageSection className={'use-and-privacy-agreement'}>
      <PageSection
        name={'header'}
        className={'use-and-privacy-agreement-header'}
      >
        <Text
          className={'bold-text'}
          style={{ height: 'fit-content' }}
          children={t('use_and_privacy_agreement.title')}
          component="h1"
        />
        <Button
          variant="plain"
          aria-label="Action"
          id={'terms_and_conditions_close_button'}
          style={{ display: isProtected ? 'block' : 'none' }}
          onClick={handleModalToggle}
          children={<TimesIcon />}
        />
      </PageSection>
      <PageSection
        name={'scrolling-section'}
        hasOverflowScroll
        tabIndex={0}
        className={'pf-m-fill'}
        style={{ height: '80%' }}
        aria-label={t('use_and_privacy_agreement.item_section').toString()}
      >
        <Text
          className={'bold-text'}
          component="h1"
          children={t('use_and_privacy_agreement.card_body.title')}
        />
        <br />
        <List component={ListComponent.ol} type={OrderType.number}>
          <ListItem className={'bold-text'}>
            {t(
              'use_and_privacy_agreement.card_body.list_conditions.who_can_use_colmena.item',
            )}
          </ListItem>
          {t(
            'use_and_privacy_agreement.card_body.list_conditions.who_can_use_colmena.response',
          )}
          <ListItem className={'bold-text'}>
            {t(
              'use_and_privacy_agreement.card_body.list_conditions.what_does_colmena_offer.item',
            )}
          </ListItem>
          {t(
            'use_and_privacy_agreement.card_body.list_conditions.what_does_colmena_offer.response',
          )}
          <ListItem className={'bold-text'}>
            {t(
              'use_and_privacy_agreement.card_body.list_conditions.how_to_create_an_account.item',
            )}
          </ListItem>
          {t(
            'use_and_privacy_agreement.card_body.list_conditions.how_to_create_an_account.response',
          )}
          <ListItem className={'bold-text'}>
            {t(
              'use_and_privacy_agreement.card_body.list_conditions.using_colmena.item',
            )}
          </ListItem>
          {t(
            'use_and_privacy_agreement.card_body.list_conditions.using_colmena.response',
          )}
          <ListItem className={'bold-text'}>
            {t(
              'use_and_privacy_agreement.card_body.list_conditions.content.item',
            )}
          </ListItem>
          {t(
            'use_and_privacy_agreement.card_body.list_conditions.content.response',
          )}
          <ListItem className={'bold-text'}>
            {t(
              'use_and_privacy_agreement.card_body.list_conditions.liability_and_account_suspension.item',
            )}
          </ListItem>
          {t(
            'use_and_privacy_agreement.card_body.list_conditions.liability_and_account_suspension.response',
          )}
          <ListItem className={'bold-text'}>
            {t(
              'use_and_privacy_agreement.card_body.list_conditions.termination_of_colmena.item',
            )}
          </ListItem>
          {t(
            'use_and_privacy_agreement.card_body.list_conditions.termination_of_colmena.response',
          )}
          <ListItem className={'bold-text'}>
            {t(
              'use_and_privacy_agreement.card_body.list_conditions.dispute_resolution.item',
            )}
          </ListItem>
          {t(
            'use_and_privacy_agreement.card_body.list_conditions.dispute_resolution.response',
          )}
          <ListItem className={'bold-text'}>
            {t(
              'use_and_privacy_agreement.card_body.list_conditions.modification_of_service_guidelines.item',
            )}
          </ListItem>
          {t(
            'use_and_privacy_agreement.card_body.list_conditions.modification_of_service_guidelines.response',
          )}
        </List>
        <br />
        <Trans
          i18nKey={'use_and_privacy_agreement.card_body.final_comment'}
          components={{ b: <strong /> }}
        />
      </PageSection>
      <PageSection
        className={'pf-m-no-fill use-and-privacy-agreement-footer'}
        style={{ display: isProtected ? 'flex' : 'none' }}
      >
        <Button
          className={'authButton'}
          id={'accept_terms_and_conditions_button'}
          children={t('use_and_privacy_agreement.accept_button_label')}
          onClick={acceptTermsAndConditions}
        />
      </PageSection>
      <Modal
        aria-label={'post_send_invitation_modal'}
        variant={ModalVariant.small}
        isOpen={isModalOpen}
        showClose={false}
      >
        <ModalBoxBody
          children={t('use_and_privacy_agreement.modal_notification.body')}
        />
        <ModalBoxFooter className={'tac-modal-footer'}>
          <Button
            className={'authButton'}
            children={t('use_and_privacy_agreement.modal_notification.back')}
            onClick={handleModalToggle}
          />
          <Button
            variant={'plain'}
            className={'cancelButton'}
            children={t('use_and_privacy_agreement.modal_notification.exit')}
            onClick={backToLogin}
          />
        </ModalBoxFooter>
      </Modal>
      <BackToTop
        className={'use-and-privacy-agreement-back-to-top'}
        isAlwaysVisible={isAlwaysVisible}
        scrollableSelector={'[name="scrolling-section"]'}
        aria-label={t('use_and_privacy_agreement.back_to_top').toString()}
      />
    </PageSection>
  );
};
