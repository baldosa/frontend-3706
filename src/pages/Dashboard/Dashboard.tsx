import * as React from 'react';
import { useState, useEffect } from 'react';
import { PageSection, Title, List, ListItem } from '@patternfly/react-core';
import { client } from '../../api/Client.ts';
import { useTranslation } from 'react-i18next';
import type { Components } from '../../api/utilities/Definitions';

const Dashboard: React.FunctionComponent = () => {
  const [users, setUsers] = useState<Components.Schemas.User[]>([]);
  const { i18n } = useTranslation();
  document.body.dir = i18n.dir();

  useEffect(() => {
    const fetchUsers = async () => {
      const response = await client.users_list();
      setUsers(response.data);
    };
    fetchUsers();
  }, []);

  return (
    <PageSection>
      <Title headingLevel="h1" size="lg">
        {''}
      </Title>
      <List>
        {users.slice(0, 10).map((item: Components.Schemas.User) => (
          <ListItem key={item.id}>{item.email}</ListItem>
        ))}
      </List>
    </PageSection>
  );
};

export { Dashboard };
