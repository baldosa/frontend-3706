import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { I18nextProvider } from 'react-i18next';
import { Dashboard } from './Dashboard';

it('feature tour', async () => {
  // @ts-expect-error type
  const test_i18n = i18n;
  render(
    <I18nextProvider i18n={test_i18n}>
      <Dashboard />
    </I18nextProvider>,
  );
});
