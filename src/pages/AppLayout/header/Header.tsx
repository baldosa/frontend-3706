import * as React from 'react';
import {
  Button,
  Grid,
  GridItem,
  Icon,
  Masthead,
  MastheadContent,
  MastheadToggle,
  PageHeader,
  PageToggleButton,
  Title,
} from '@patternfly/react-core';
import logo from '/logoipsum.png';
import { useNavigate, useLocation } from 'react-router-dom';
import { ContextProps, ContextPropsProvider } from '../AppLayout.tsx';
import { homeFullPath } from '../../../routes.tsx';
import { recorderFullPath, editorFullPath } from '../../../routes.tsx';
import { Breadcrumb } from '@patternfly/react-core';
import AngleLeftIcon from '@patternfly/react-icons/dist/esm/icons/angle-left-icon';
import AbortAudioRecordingModal from '../../../components/Waveform/ui/AbortAudioRecordingModal';
import { useWaveformEmitter } from '../../../hooks/useWaveformEmitter';
import { useSelector } from 'react-redux';
import { getActiveRecordingState } from '../../../redux/reducers/recordings';
import { PlaylistStatusEnum } from '../../../enums';
import { convertBase64ToBlobURL } from '../../../utils/logics/fileUtils.ts';
import { getOrganizationLogo } from '../../../redux/reducers/configuration/initial/organizationDetails.ts';
import { FunctionComponent } from 'react';

export const GenericHeader: React.FunctionComponent<{
  mainTitle: string;
  secondaryTitle: string;
  lightModeIsActive: boolean;
  breadCrumb?: React.ReactElement[];
  actionMenus?: React.ReactElement[];
  contextProps: ContextProps | undefined;
  navigationDestination?: string;
}> = (props: {
  mainTitle: string;
  secondaryTitle: string;
  lightModeIsActive: boolean;
  breadCrumb?: React.ReactElement[];
  actionMenus?: React.ReactElement[];
  contextProps: ContextProps | undefined;
  navigationDestination?: string;
}): React.ReactElement => {
  const navigate = useNavigate();
  const location = useLocation();
  const [isModalOpen, setModalOpen] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);

  const { emitterClear } = useWaveformEmitter({
    emitter: props.contextProps?.emitter,
  });

  const handleBackPage = () => {
    const currentPage = [recorderFullPath(), editorFullPath()];

    // If the user on the recording screen presses the back button on the screen,
    // the playlist must be removed from the screen and the audio status updated
    if (
      ![PlaylistStatusEnum.NONE].includes(audioState) &&
      currentPage.includes(location.pathname)
    )
      setModalOpen(true);
    else navigateBack();
  };

  function handleClose() {
    setModalOpen(false);
  }

  function navigateBack() {
    navigate(-1);
  }

  function onAbortAudioRecording() {
    setIsLoading(true);
    emitterClear();

    setTimeout(() => {
      setModalOpen(false);
      setIsLoading(false);
      navigateBack();
    }, 1000);
  }

  return (
    <>
      <AbortAudioRecordingModal
        isModalOpen={isModalOpen}
        handleClose={handleClose}
        onConfirm={onAbortAudioRecording}
        isLoading={isLoading}
      />
      <Masthead
        className={`${
          props.breadCrumb && 'light-header'
        } pf-c-masthead pf-m-display-inline`}
        style={{
          overflow: 'hidden',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
        }}
      >
        {props.breadCrumb && (
          <Breadcrumb style={{ height: '30px' }}>{props.breadCrumb}</Breadcrumb>
        )}
        <MastheadContent
          style={{ display: 'flex', justifyContent: 'flex-start' }}
        >
          <MastheadToggle>
            <PageToggleButton variant={'plain'} onClick={handleBackPage}>
              <Icon>
                <AngleLeftIcon className={'back-button'} />
              </Icon>
            </PageToggleButton>
          </MastheadToggle>
          <Grid>
            <GridItem>
              <Button
                isDisabled={
                  props.navigationDestination === undefined ||
                  props.navigationDestination.length === 0
                }
                variant={'plain'}
                onClick={(): void => {
                  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                  navigate(props.navigationDestination!);
                }}
              >
                <Title
                  className={'title'}
                  headingLevel={'h2'}
                  children={props.mainTitle}
                />
                <Title
                  className={'title'}
                  headingLevel={'h6'}
                  children={props.secondaryTitle}
                />
              </Button>
            </GridItem>
          </Grid>
          {props.actionMenus && (
            <ContextPropsProvider
              contextProps={props.contextProps}
              children={props.actionMenus}
            />
          )}
        </MastheadContent>
      </Masthead>
    </>
  );
};

export const MainHeader: FunctionComponent<{
  onNavToggle: (() => void) | undefined;
}> = (props) => {
  const organizationLogo: string | null = useSelector(getOrganizationLogo);
  const navigate = useNavigate();

  return (
    <PageHeader
      logo={
        <img
          src={
            organizationLogo ? convertBase64ToBlobURL(organizationLogo) : logo
          }
          alt="Colmena Logo"
          onClick={() => navigate(homeFullPath())}
          style={{ maxWidth: '135px', maxHeight: '35px', borderRadius: '0.5rem' }}
        />
      }
      showNavToggle
      onNavToggle={props.onNavToggle}
    />
  );
};
