import * as React from 'react';
import { Button, Card, ListItem } from '@patternfly/react-core';
import { useNavigate } from 'react-router-dom';
import { Fragment } from 'react';

export const ToolItem: React.FunctionComponent<{
  icon: React.ReactElement;
  name: string;
  url: string;
  forceReloadPage?: boolean;
}> = (props) => {
  const navigate = useNavigate();

  /* 
  To access the audio recorder/editor screen, completely reloading the page is essential 
  to prevent event-emitter listeners from being created more than once. Therefore, this 
  forceReloadPage property, once set to true, will open the page through location.href.
  */
  const handleNavigate = () => {
    const { url } = props;
    if (!props.forceReloadPage) navigate(url);
    else location.href = url;
  };

  return (
    <ListItem className={'list-item tool-item'}>
      <Button variant={'plain'} onClick={handleNavigate}>
        <Card
          className={'tool-item'}
          isRounded
          isPlain
          children={
            <Fragment>
              {props.icon}
              <span className={'tool-name'} children={<b>{props.name}</b>} />
            </Fragment>
          }
        />
      </Button>
    </ListItem>
  );
};
