import { PLAYBACK_POSITION, getTextInputValue } from '../utils/waveformUtils';
import { useDispatch, useSelector } from 'react-redux';
import {
  updateRecordingState,
  setRecordingCounter,
  setRecordingName,
  setEditorOptionsVisibility,
} from '../redux/actions/recordings';
import {
  PlaylistStatusEnum,
  // PlaylistContextEnum,
  HandleInteractionWaveformEnum,
  StatesInteractionWaveformEnum,
} from '../enums/index';
import { ContextProps } from '../pages/AppLayout/AppLayout';
import { useTranslation } from 'react-i18next';
import { generateRecordingName } from '../utils/utils';
import {
  getActiveRecordingState,
  getRecordingCounter,
  // getEditorContext,
} from '../redux/reducers/recordings';
import { getMicrophonePermission } from '../utils/utils';
import {
  StatesInteractionWaveformProps,
  HandleInteractionWaveformProps,
} from '../types';

export const emitterFactory = () => {
  return {
    emit() {
      console.warn('Some error happened with EventEmitter');
    },
    on() {
      console.warn('Some error happened with EventEmitter');
    },
  };
};

export const useWaveformEmitter = ({ emitter }: ContextProps) => {
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);
  const recordingCounter: number = useSelector(getRecordingCounter);

  const ee = !emitter ? emitterFactory() : emitter;

  const emitterZoomIn = () => ee.emit('zoomin');
  const emitterZoomOut = () => ee.emit('zoomout');

  const emitterStop = () => {
    dispatch(updateRecordingState(PlaylistStatusEnum.STOPPED));
    dispatch(setRecordingName(generateRecordingName(i18n.language)));

    ee.emit('stop');
    ee.emit('commit');
  };

  const emitterPlay = () => {
    if (audioState !== PlaylistStatusEnum.PAUSED) ee.emit('rewind');
    dispatch(updateRecordingState(PlaylistStatusEnum.PLAYING));
    setTimeout(() => ee.emit('play'), 500);
  };

  const emitterPause = () => {
    dispatch(updateRecordingState(PlaylistStatusEnum.PAUSED));
    ee.emit('pause');
  };

  const emitterPauseRecording = () => {
    const audioState_ =
      audioState === PlaylistStatusEnum.RECORDING
        ? PlaylistStatusEnum.RECORDING_PAUSED
        : PlaylistStatusEnum.RECORDING;
    dispatch(updateRecordingState(audioState_));
    ee.emit('pause');
  };

  const emitterRecord = async () => {
    const micPer = await getMicrophonePermission();

    if (micPer === 'no') {
      alert(t('micPermissionTitle'));
      return;
    }

    dispatch(setRecordingCounter(recordingCounter + 1));
    dispatch(updateRecordingState(PlaylistStatusEnum.RECORDING));
    dispatch(setEditorOptionsVisibility(false));

    ee.emit('record');
  };

  const emitterSelect = () => ee.emit('select');

  const emitterClear = () => {
    ee.emit('clear');
    setTimeout(() => {
      dispatch(updateRecordingState(PlaylistStatusEnum.NONE));
      dispatch(setRecordingCounter(0));
      dispatch(setEditorOptionsVisibility(false));
    }, 1000);
  };

  const emitterRenderingBuffer = () => ee.emit('startaudiorendering', 'buffer');
  const emitterRenderingWav = () => ee.emit('startaudiorendering', 'wav');
  const emitterRenderingOpus = () => ee.emit('startaudiorendering', 'opus');

  const emitterRewind = (ftr = 0) => {
    dispatch(updateRecordingState(PlaylistStatusEnum.PLAYING));
    if (ftr === 0) ee.emit('rewind');
    else {
      const playbackPosition = Number(getTextInputValue(PLAYBACK_POSITION));
      if (playbackPosition >= 0) {
        if (playbackPosition > ftr) ee.emit('play', playbackPosition - ftr);
        else ee.emit('play', 0);
      }
    }
  };

  const emitterForward = (ftr = 0) => {
    dispatch(updateRecordingState(PlaylistStatusEnum.PLAYING));
    if (ftr === 0) ee.emit('forward');
    else {
      const playbackPosition = Number(getTextInputValue(PLAYBACK_POSITION));
      ee.emit('play', playbackPosition + ftr);
    }
  };

  const emitterMasterVolumeChange = (newValue: number | number[]) =>
    ee.emit('mastervolumechange', newValue);

  const emitterExportProject = () => ee.emit('exportZipProject');

  const emitterImportAudio = (blob: Blob) => ee.emit('newtrack', blob);
  const emitterImportProject = (blob: Blob) =>
    ee.emit('importZipProject', blob);

  const emitterStateChange = (type: StatesInteractionWaveformProps) => {
    ee.emit('statechange', type);

    if (
      type === StatesInteractionWaveformEnum.FADEIN ||
      type === StatesInteractionWaveformEnum.FADEOUT
    )
      ee.emit('commit');
  };

  const emitterHandleTrack = (type: HandleInteractionWaveformProps) => {
    if (type === HandleInteractionWaveformEnum.SPLIT)
      dispatch(setRecordingCounter(recordingCounter + 1));

    ee.emit(type);
    ee.emit('commit');
  };

  const emitterRedo = () => {
    ee.emit('redo');
  };

  const emitterUndo = () => {
    ee.emit('undo');
  };

  const emitterCommit = () => {
    ee.emit('commit');
  };

  return {
    emitter: ee,
    emitterRecord,
    emitterRewind,
    emitterForward,
    emitterPause,
    emitterPauseRecording,
    emitterPlay,
    emitterStop,
    emitterZoomIn,
    emitterZoomOut,
    emitterSelect,
    emitterMasterVolumeChange,
    emitterRenderingBuffer,
    emitterRenderingWav,
    emitterClear,
    emitterExportProject,
    emitterRenderingOpus,
    emitterImportAudio,
    emitterImportProject,
    emitterStateChange,
    emitterHandleTrack,
    emitterCommit,
    emitterRedo,
    emitterUndo,
  };
};
