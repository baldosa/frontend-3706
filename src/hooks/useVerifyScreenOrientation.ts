import { useEffect, useState } from 'react';

export const useVerifyScreenOrientation = () => {
  const [isPortraitView, setIsPortraitView] = useState(true);

  const verifyScreenOrientation = () => {
    if (window.innerHeight > window.innerWidth) {
      setIsPortraitView(true);
    } else {
      setIsPortraitView(false);
    }
  };

  useEffect(() => {
    verifyScreenOrientation();
    window.addEventListener(
      'resize',
      () => {
        verifyScreenOrientation();
      },
      false,
    );
  }, []);

  return {
    isPortraitView,
  };
};
