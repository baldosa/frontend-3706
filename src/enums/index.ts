export enum AudioExportTypesEnum {
  MP3 = 'mp3',
  WAV = 'wav',
  WEBA = 'weba',
  OPUS = 'opus',
  AAC = 'aac',
  ZIP = 'zip',
}

export enum AudioGlobalConfigEnum {
  SAMPLE_RATE = 48000,
}

export enum DirectoryNamesNCEnum {
  TALK = 'Talk',
}

export enum PlaylistStatusEnum {
  STOPPED = 'STOPPED',
  PAUSED = 'PAUSED',
  PLAYING = 'PLAYING',
  RECORDING = 'RECORDING',
  RECORDING_PAUSED = 'RECORDING_PAUSED',
  NONE = 'NONE',
  IMPORTED_AUDIO = 'IMPORTED_AUDIO',
}

export enum PlaylistContextEnum {
  RECORDER = 'recorder',
  EDITOR = 'editor',
}

export enum AudioRenderingContextEnum {
  UPLOAD = 'upload',
  DOWNLOAD = 'download',
}

export enum ImportAudioContextEnum {
  DEVICE = 'device',
  SERVER = 'server',
}

export enum AudioTypeImportEnum {
  SINGLE_AUDIO = 'single_audio',
  AUDIO_PROJECT = 'audio_project',
}

export enum StatesInteractionWaveformEnum {
  CURSOR = 'cursor',
  SELECT = 'select',
  FADEIN = 'fadein',
  FADEOUT = 'fadeout',
  SHIFT = 'shift',
}

export enum HandleInteractionWaveformEnum {
  TRIM = 'trim',
  CUT = 'cut',
  SPLIT = 'split',
}
