import {
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  REGISTER_TAC_CONFIRM_ACCEPTANCE,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  LOGOUT,
  UPDATE_ACCESS_TOKEN,
  REGISTER_TAC_CANCEL_ACCEPTANCE,
  SAVE_AUTH_DATA,
} from '../types/types.ts';
import { AuthService } from '../../service/user/AuthService.ts';
import { Components, Paths } from '../../api/utilities/Definitions';
import { AxiosResponse } from 'axios';
import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';

export const login =
  (username: string, password: string) => (dispatch: Dispatch<AnyAction>) => {
    return AuthService.login(username, password).then(
      (response: AxiosResponse<Components.Schemas.JWT, unknown>) => {
        dispatch({ type: LOGIN_SUCCESS, payload: { user: response.data } });
        return Promise.resolve();
      },
      (error) => {
        dispatch({ type: LOGIN_FAIL });
        console.log(error);
        return Promise.reject(error);
      },
    );
  };

export const signup =
  (
    parameters: Paths.ConfirmInvitation.PathParameters,
    username: string,
    password: string,
  ) =>
  (dispatch: Dispatch<AnyAction>) => {
    return AuthService.signup(parameters, username, password).then(
      () => {
        dispatch({ type: REGISTER_SUCCESS });
        return Promise.resolve();
      },
      (error) => {
        dispatch({ type: REGISTER_FAIL });
        return Promise.reject(error);
      },
    );
  };

export const saveUserData =
  (username: string | null, password: string | null) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: SAVE_AUTH_DATA,
      payload: {
        user: { user: { email: username, password: password } },
      },
    });
  };

export const confirmTAC =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: REGISTER_TAC_CONFIRM_ACCEPTANCE,
    });
  };

export const cancelTAC =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: REGISTER_TAC_CANCEL_ACCEPTANCE,
    });
  };

export const updateAccessToken = (
  accessToken: string | undefined,
  dispatch: Dispatch<AnyAction>,
) => {
  return dispatch({
    type: UPDATE_ACCESS_TOKEN,
    payload: { access: accessToken },
  });
};

export const logout =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({ type: LOGOUT });
  };

export const passwordReset = (email: string) => {
  return AuthService.passwordReset(email);
};

const getTokenAndUID = (fullToken: string) => {
  const uid = fullToken.substring(0, fullToken.indexOf('-'));
  const token = fullToken.replace(uid + '-', '');
  return { token, uid };
};

export const passwordResetConfirm = (
  paramsToken: string,
  newPassword: string,
  newPasswordConfirm: string,
) => {
  const { token, uid } = getTokenAndUID(paramsToken);
  return AuthService.passwordResetConfirm(
    uid,
    token,
    newPassword,
    newPasswordConfirm,
  );
};
