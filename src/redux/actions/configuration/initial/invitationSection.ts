import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import {
  ADD_INVITATION,
  COMPLETED_INVITATION_SECTION,
  INVITATION_SECTION_NO_REDIRECTION_REQUIRED,
  REMOVE_INVITATION,
  UNCOMPLETED_INVITATION_SECTION,
} from '../../../types/types.ts';
import { Invitation } from '../../../../pages/User/Configuration/Invitation/InvitationItem.tsx';

export const addInvitation =
  (invitation: Invitation) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: ADD_INVITATION,
      payload: {
        invitation: invitation,
      },
    });
  };

export const removeInvitation =
  (id: number) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: REMOVE_INVITATION,
      payload: {
        id: id,
      },
    });
  };

export const invitationSectionCompleted =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: COMPLETED_INVITATION_SECTION,
    });
  };

export const invitationSectionUncompleted =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: UNCOMPLETED_INVITATION_SECTION,
    });
  };

export const endRedirection =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: INVITATION_SECTION_NO_REDIRECTION_REQUIRED,
    });
  };
