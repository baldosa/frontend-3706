import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import {
  USER_PROFILE_COMPLETED,
  USER_PROFILE_LOADED,
  USER_PROFILE_LOADED_COMPLETELY,
  USER_PROFILE_NO_REDIRECTION_REQUIRED,
} from '../../../types/types.ts';

export const changeUserProfileUsername =
  (username: string | undefined) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: USER_PROFILE_LOADED,
      payload: {
        username,
      },
    });
  };

export const changeUserProfileEmail =
  (email: string | undefined) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: USER_PROFILE_LOADED,
      payload: {
        email,
      },
    });
  };

export const changeUserProfileAvatar =
  (avatar: string | null) =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: USER_PROFILE_LOADED,
      payload: {
        avatar,
      },
    });
  };

export const userProfileLoadedCompletely =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: USER_PROFILE_LOADED_COMPLETELY,
    });
  };

export const userProfileCompleted =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: USER_PROFILE_COMPLETED,
    });
  };

export const endRedirection =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: USER_PROFILE_NO_REDIRECTION_REQUIRED,
    });
  };
