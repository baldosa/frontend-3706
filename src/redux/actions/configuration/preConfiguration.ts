import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import {
  PRE_CONFIGURATION_COMPLETE_STEP,
  PRE_CONFIGURATION_LOAD_DATA,
} from '../../types/types.ts';

export const preConfigurationLoadData =
  () =>
  (dispatch: Dispatch<AnyAction>): void => {
    dispatch({
      type: PRE_CONFIGURATION_LOAD_DATA,
    });
  };

export const preConfigurationCompleteStep =
  (stepId: string) =>
  (dispatch: Dispatch<AnyAction>): boolean => {
    dispatch({
      type: PRE_CONFIGURATION_COMPLETE_STEP,
      payload: {
        stepId: stepId,
      },
    });
    return true;
  };
