import {
  InitialConfigurationType,
  ORGANIZATION_DETAILS_COMPLETED,
  ORGANIZATION_DETAILS_FAILED_LOAD,
  ORGANIZATION_DETAILS_LOADED,
  ORGANIZATION_DETAILS_LOADED_COMPLETELY,
  ORGANIZATION_DETAILS_NO_REDIRECTION_REQUIRED,
  ORGANIZATION_UPDATE_LOGO,
} from '../../../types/types.ts';
import { RootState } from '../../../store.ts';

export const organizationStepId = 'wizard_organization_step';

interface OrganizationDetails {
  data: {
    name: string | null;
    email: string | null;
    logo: string | null;
    country: { key: string; value: string } | null;
    language: { key: string; value: string } | null;
    image: string | null;
  };
  isLoadedCompletely: boolean | null;
  requiresRedirection: boolean;
  isCompleted: boolean;
}

const initialState: OrganizationDetails = {
  data: {
    name: null,
    email: null,
    country: null,
    language: null,
    image: null,
    logo: null,
  },
  requiresRedirection: true,
  isLoadedCompletely: false,
  isCompleted: false,
};

export default function (
  state: OrganizationDetails = initialState,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  action: { type: InitialConfigurationType; payload: any },
): OrganizationDetails {
  const { type, payload } = action;

  switch (type) {
    case ORGANIZATION_DETAILS_LOADED:
      return {
        ...state,
        data: {
          name: payload.name ? payload.name : state.data.name,
          email: payload.email ? payload.email : state.data.email,
          logo: payload.logo ? payload.logo : state.data.logo,
          country: payload.country ? payload.country : state.data.country,
          language: payload.language ? payload.language : state.data.language,
          image: payload.image ? payload.image : state.data.image,
        },
      };
    case ORGANIZATION_UPDATE_LOGO:
      return {
        ...state,
        data: {
          ...state.data,
          logo: payload.logo
        },
      };
    case ORGANIZATION_DETAILS_LOADED_COMPLETELY:
      return {
        ...state,
        isLoadedCompletely: true,
      };
    case ORGANIZATION_DETAILS_FAILED_LOAD:
      return {
        ...state,
        isLoadedCompletely: null,
      };
    case ORGANIZATION_DETAILS_COMPLETED:
      return {
        ...state,
        isCompleted: true,
      };
    case ORGANIZATION_DETAILS_NO_REDIRECTION_REQUIRED:
      return {
        ...state,
        requiresRedirection: false,
      };
    default:
      return state;
  }
}

export const getOrganizationName = (state: RootState) =>
  state.organizationDetails.data.name;
export const getOrganizationEmail = (state: RootState) =>
  state.organizationDetails.data.email;
export const getOrganizationCountry = (state: RootState) =>
  state.organizationDetails.data.country;
export const getOrganizationLogo = (state: RootState) =>
  state.organizationDetails.data.logo;
export const getOrganizationLanguage = (state: RootState) =>
  state.organizationDetails.data.language;
export const getImage = (state: RootState) =>
  state.organizationDetails.data.image;
export const getIsCompleted = (state: RootState) =>
  state.organizationDetails.isCompleted;
export const getIsLoadedCompletely = (state: RootState) =>
  state.organizationDetails.isLoadedCompletely;
export const getRequiresRedirectionStatus = (state: RootState) =>
  state.organizationDetails.requiresRedirection;
