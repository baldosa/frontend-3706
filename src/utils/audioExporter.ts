/* eslint-disable @typescript-eslint/no-explicit-any */
import WebMMuxer from 'webm-muxer';
import { saveAs } from 'file-saver';
import { AudioExportTypesEnum, AudioGlobalConfigEnum } from '../enums/index';
import { AudioExportTypesProps } from '../types';

declare global {
  interface Window {
    AudioEncoder: any;
    AudioData: any;
  }
}

let recLength = 0;
const recBuffersL: Float32Array[] = [];
const recBuffersR: Float32Array[] = [];
const sampleRate = AudioGlobalConfigEnum.SAMPLE_RATE;
let filename: string;

export function init(inputBuffer: Float32Array[], flname: string) {
  filename = flname;
  recBuffersL.push(inputBuffer[0]);
  recBuffersR.push(inputBuffer[1]);
  recLength += inputBuffer[0].length;
}

function floatTo16BitPCM(
  output: DataView,
  offset: number,
  input: Float32Array,
) {
  let writeOffset = offset;
  for (let i = 0; i < input.length; i += 1, writeOffset += 2) {
    const s = Math.max(-1, Math.min(1, input[i]));
    output.setInt16(writeOffset, s < 0 ? s * 0x8000 : s * 0x7fff, true);
  }
}

function mergeBuffers(recBuffers: Float32Array[], length: number) {
  const result = new Float32Array(length);
  let offset = 0;

  for (let i = 0; i < recBuffers.length; i += 1) {
    result.set(recBuffers[i], offset);
    offset += recBuffers[i].length;
  }
  return result;
}

export async function exportOpus() {
  const channels = 2;
  let muxer: any = null;
  let total_encoded_size: number;
  total_encoded_size = 0;

  muxer = new WebMMuxer({
    target: 'buffer',
    audio: {
      codec: 'A_OPUS',
      sampleRate,
      numberOfChannels: channels,
    },
  });

  const encoder = new window.AudioEncoder({
    error(e: Error) {
      console.log(e);
    },
    output(chunk: any, meta: any) {
      total_encoded_size += chunk.byteLength;
      muxer.addAudioChunk(chunk, meta);
    },
  });
  /* hack to avoid the message ''total_encoded_size' is declared but its value is never read' when run the build command.
  Apparently, the total_encoded_size variable within the output method is not being considered in ts type-check validation
  */
  console.log(total_encoded_size);
  /* ------------------------------- */

  const config = {
    numberOfChannels: channels,
    sampleRate,
    codec: 'opus',
    bitrate: 64000,
  };

  encoder.configure(config);

  const bufferL = mergeBuffers(recBuffersL, recLength);
  const bufferR = mergeBuffers(recBuffersR, recLength);

  const bufferL3 = new ArrayBuffer(recLength * 2);
  const bufferR3 = new ArrayBuffer(recLength * 2);

  const samplesL = new DataView(bufferL3);
  const samplesR = new DataView(bufferR3);

  floatTo16BitPCM(samplesL, 0, bufferL);
  floatTo16BitPCM(samplesR, 0, bufferR);

  const Mp3L = new Int16Array(bufferL3, 0, recLength);
  const Mp3R = new Int16Array(bufferR3, 0, recLength);

  let remaining = recLength;
  const samplesPerFrame = 1024;
  let base_time = 0;

  for (let i = 0; remaining >= samplesPerFrame; i += samplesPerFrame) {
    const left = Mp3L.subarray(i, i + samplesPerFrame);
    const right = Mp3R.subarray(i, i + samplesPerFrame);
    const planar_data = new Int16Array(samplesPerFrame * channels);

    planar_data.set(left, 0);
    planar_data.set(right, samplesPerFrame);

    base_time = (i * samplesPerFrame) / sampleRate;
    const audio_data = new window.AudioData({
      timestamp: 1000000 * base_time,
      data: planar_data,
      numberOfChannels: channels,
      numberOfFrames: samplesPerFrame,
      sampleRate,
      format: 's16-planar',
    });
    encoder.encode(audio_data);

    remaining -= samplesPerFrame;
  }

  if (remaining > 0) {
    const left = Mp3L.subarray(recLength - remaining, recLength);
    const right = Mp3R.subarray(recLength - remaining, recLength);
    const planar_data = new Int16Array(remaining * channels);

    planar_data.set(left, 0);
    planar_data.set(right, remaining);

    base_time += samplesPerFrame;

    const audio_data = new window.AudioData({
      timestamp: 1000000 * base_time,
      data: planar_data,
      numberOfChannels: channels,
      numberOfFrames: remaining,
      sampleRate,
      format: 's16-planar',
    });
    encoder.encode(audio_data);
    remaining = 0;
  }

  await encoder.flush();
  const buffer = muxer.finalize();

  const audioBlob = new Blob([buffer], { type: 'audio/webm' });
  saveAs(audioBlob, `${filename}.${AudioExportTypesEnum.WEBA}`);
}

export async function initAudioExporter(
  audioBuffer: AudioBuffer,
  filename: string,
  type: AudioExportTypesProps,
) {
  init(
    [audioBuffer.getChannelData(0), audioBuffer.getChannelData(1)],
    filename,
  );

  switch (type) {
    case AudioExportTypesEnum.OPUS: {
      await exportOpus();
      break;
    }
    default: {
      console.log('Unknown export command');
      break;
    }
  }
}
