// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import $ from 'jquery';
import { clockFormatWaveformPlaylist } from './utils';

export function getTextInputValue(id: string): number | string {
  const elem = document.getElementById(id) as HTMLInputElement;

  if (elem === null) return 0;

  const val = String(elem.value);

  if (Number.isNaN(Number(val))) return String(val);

  return Number(val);
}

export function setTextInputValue(id: string, val: number | string) {
  $(`#${id}`).val(String(val));
  $(`#playback-position-formatted`).text(
    String(clockFormatWaveformPlaylist(Number(val), 0)),
  );
}

export function addClassToPlaylistCursor(cls: string) {
  $('.playlist .cursor').removeClass('cursor-recording');
  $('.playlist .cursor').removeClass('cursor-recording-paused');
  $('.playlist .cursor').removeClass('cursor-recording-stopped');
  $('.playlist .cursor').addClass(cls);
}

export const PLAYBACK_POSITION = 'playback-position';
export const RECORDING_COUNTER = 'recording-counter';
export const RECORDING_NAME = 'recording-name';
export const AUDIO_RENDERING_CONTEXT = 'audio-rendering-context';
