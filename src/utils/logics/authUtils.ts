import { NavigateFunction } from 'react-router-dom';
import { saveUserData } from '../../redux/actions/auth.ts';
import { signupTACFullPath } from '../../routes.tsx';
import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';

export const comesFromAccountCreationToTac = (
  username: string | null,
  password: string | null,
  dispatch: Dispatch<AnyAction>,
  navigate: NavigateFunction,
) => {
  saveUserData(username, password)(dispatch);
  navigate(signupTACFullPath());
};
