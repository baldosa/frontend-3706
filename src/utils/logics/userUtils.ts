import { UserService } from '../../service/user/UserService.ts';
import {
  changeUserProfileAvatar,
  changeUserProfileEmail,
  changeUserProfileUsername,
  userProfileLoadedCompletely,
} from '../../redux/actions/configuration/initial/userProfileDetails.ts';
import { Dispatch } from 'react';
import { AnyAction } from '@reduxjs/toolkit';
import { AuthService } from '../../service/user/AuthService.ts';
import { updateAccessToken } from '../../redux/actions/auth.ts';
import { Role } from '../../service/user/Role.ts';

export const saveUserData = (
  userId: number,
  userToken: string,
  dispatch: Dispatch<AnyAction>,
) => {
  UserService.getUserData(userId, userToken)
    .then((response) => {
      const avatar: string | undefined = response.data.avatar;
      changeUserProfileUsername(response.data.full_name)(dispatch);
      changeUserProfileEmail(response.data.email)(dispatch);
      changeUserProfileAvatar(
        avatar !== undefined && avatar.length > 0 ? avatar : null,
      )(dispatch);
      userProfileLoadedCompletely()(dispatch);
    })
    .catch((error) => {
      console.error(error);
    });
};

export const hasAnyRole = (userRoles: string[], requiredRoles: string[]) => {
  return userRoles.some((role: string): boolean => {
    return requiredRoles.length == 0 || requiredRoles.includes(role);
  });
};

export const isAdministrator = (userRoles: string[]) => {
  return (
    userRoles &&
    userRoles.some((role: string): boolean => {
      return role == Role.Admin || role == Role.OrgOwner;
    })
  );
};

export const verifyAndRefreshToken = async (
  refreshToken: string,
  accessToken: string,
  dispatch: Dispatch<AnyAction>,
) => {
  return AuthService.verifyToken(refreshToken)
    .then(async () => {
      return await AuthService.verifyToken(accessToken)
        .then(async () => {
          return await AuthService.refreshToken(refreshToken)
            .then(async (response) => {
              updateAccessToken(response.data.access, dispatch);
              return await Promise.resolve();
            })
            .catch((error) => {
              console.error(error);
              return Promise.reject(error);
            });
        })
        .catch((error) => {
          console.error(error);
          return Promise.reject(error);
        });
    })
    .catch((error) => {
      console.error(error);
      return Promise.reject(error);
    });
};
