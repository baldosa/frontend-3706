export class IndexedDBManager {
  private static databases: { [name: string]: { version: number } } = {};

  public static createObjectStore(
    dataBaseName: string,
    objectStoreName: string,
  ): Promise<IDBDatabase> {
    return new Promise((resolve, reject) => {
      const currentVersion =
        IndexedDBManager.getCurrentVersion(dataBaseName) + 1;
      IndexedDBManager.createDataBase(dataBaseName, currentVersion)
        .then((database: IDBDatabase) => {
          database.createObjectStore(objectStoreName);
          resolve(database);
        })
        .catch((error) => reject(error));
    });
  }

  public static addElementToStore(
    dataBaseName: string,
    objectStoreName: string,
    key: number,
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
    value: any,
  ): void {
    IndexedDBManager.getDataBase(dataBaseName)
      .then((database: IDBDatabase) => {
        database
          .transaction(objectStoreName, 'readwrite')
          .objectStore(objectStoreName)
          .put(value, key);
      })
      .catch((error) => console.error(error));
  }

  public static getElementToStore(
    dataBaseName: string,
    objectStoreName: string,
    key: number,
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      IndexedDBManager.getDataBase(dataBaseName)
        .then((database: IDBDatabase) => {
          const request = database
            .transaction(objectStoreName, 'readonly')
            .objectStore(objectStoreName)
            .get(key);

          request.onsuccess = () => {
            resolve(request.result);
          };

          request.onerror = () => {
            reject(`Error: Could not get ${key} element`);
          };
        })
        .catch((error) => error);
    });
  }

  private static getDataBase(name: string): Promise<IDBDatabase> {
    return new Promise((resolve, reject) => {
      const request = window.indexedDB.open(name);

      request.onsuccess = (event: Event) => {
        resolve((event.target as IDBOpenDBRequest).result);
      };

      request.onerror = () => {
        reject(`Error: Could not connect to database ${name}`);
      };
    });
  }

  private static createDataBase(
    name: string,
    version: number,
  ): Promise<IDBDatabase> {
    return new Promise((resolve, reject) => {
      const request = window.indexedDB.open(name, version);

      request.onupgradeneeded = (event: Event) => {
        const database: IDBDatabase = (event.target as IDBOpenDBRequest).result;
        IndexedDBManager.databases[name] = { version: database.version };
        resolve(database);
      };

      request.onerror = () => {
        reject(`Error: The database ${name} could not be created`);
      };
    });
  }

  private static getCurrentVersion(databaseName: string): number {
    const databaseInfo = IndexedDBManager.databases[databaseName];
    return databaseInfo ? databaseInfo.version : 0;
  }
}

export const mediaDataBase = 'Media';
export const audioStore = 'Audio';
export const visualStore = 'Visual';
export const documentStore = 'Document';
