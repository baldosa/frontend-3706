import OpenAPIClientAxios from 'openapi-client-axios';
import type { Client } from './utilities/Definitions';
import definition from './utilities/schema-runtime.json';

const baseURL = import.meta.env.VITE_BACKEND_API_URL;

const api: OpenAPIClientAxios = new OpenAPIClientAxios({
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  definition,
  axiosConfigDefaults: {
    baseURL: baseURL,
    headers: { accept: 'application/json' },
  },
});

export const buildAuthToken = (token: string): string => {
  return 'Bearer ' + token;
};

export const client: Client = await api.getClient<Client>();
