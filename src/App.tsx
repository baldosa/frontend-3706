import * as React from 'react';
import './App.css';
import '@patternfly/react-core/dist/styles/base.css';
import { RouterProvider } from 'react-router-dom';
import { Router } from './routes.tsx';

const App: React.FunctionComponent = () => <RouterProvider router={Router} />;

export default App;
