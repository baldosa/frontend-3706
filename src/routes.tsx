import { NotFound } from './pages/NotFound/NotFound';
import { AuthPage } from './pages/Auth/AuthPage.tsx';
import { RecorderDropdown, Recorder } from './pages/Tools/Recorder.tsx';
import { EditorDropdown, Editor } from './pages/Tools/Editor.tsx';
import {
  createBrowserRouter,
  createRoutesFromElements,
  Navigate,
  NavLink,
  Outlet,
  Route,
  useLocation,
  useNavigate,
} from 'react-router-dom';
import React, {
  Dispatch,
  FormEvent,
  Fragment,
  FunctionComponent,
  useEffect,
  useState,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getRoles,
  getAccessToken,
  isLoggedIn,
  getRefreshToken,
} from './redux/reducers/auth.ts';
import { TermsAndConditions } from './pages/Help/TermsAndConditions.tsx';
import {
  AppLayout,
  AuthFooterKey,
  Main,
  ActionMenus,
  TeamList as TeamListKey,
  Home as HomeKey,
  BreadCrumb,
  Tools,
} from './pages/AppLayout/AppLayout.tsx';
import { InitialConfiguration } from './pages/User/InitialConfiguration.tsx';
import { AccessToInitialConfiguration } from './pages/User/AccessToInitialConfiguration.tsx';
import {
  Nav,
  NavExpandable,
  NavItem,
  NavList,
  Text,
} from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { AuthService } from './service/user/AuthService.ts';
import { AnyAction } from '@reduxjs/toolkit';
import { logout } from './redux/actions/auth.ts';
import { UserProfileForm } from './pages/User/Configuration/UserProfileForm.tsx';
import { PasswordReset } from './pages/Auth/PasswordReset.tsx';
import { PasswordResetConfirm } from './pages/Auth/PasswordResetConfirm.tsx';
import {
  getIsLoadedCompletely,
  isCompleted,
} from './redux/reducers/configuration/preConfiguration.ts';
import { TeamList } from './pages/Team/General/TeamList.tsx';
import { TeamInfo } from './pages/Team/Parts/TeamInfo.tsx';
import { TeamFiles } from './pages/Team/Parts/File/TeamFiles.tsx';
import { TeamChat, TeamChatDropdown } from './pages/Team/Chat/TeamChat.tsx';
import { LOGIN, SIGNUP } from './components/Auth/ValidationRules.ts';
import { ColmenaIcon } from './assets/icon/ColmenaIcon.tsx';
import { ChatIcon } from './assets/icon/ChatIcon.tsx';
import { UserIcon } from './assets/icon/UserIcon.tsx';
import { SearchIcon } from './assets/icon/SearchIcon.tsx';
import { ToolsIcon } from './assets/icon/ToolsIcon.tsx';
import { Home } from './pages/Home.tsx';
import { OrganizationDetailsForm } from './pages/User/Configuration/Organization/OrganizationDetailsForm.tsx';
import { AccessTools } from './pages/Tools/AccessTools.tsx';
import { TeamService } from './service/TeamService.ts';
import EventEmitter from 'event-emitter';
import {
  hasAnyRole,
  isAdministrator,
  verifyAndRefreshToken,
} from './utils/logics/userUtils.ts';
import { AuthContent } from './components/Auth/AuthContent.tsx';
import { WelcomeSection } from './components/Auth/WelcomeSection.tsx';
import { addAlert } from './redux/actions/alert.ts';
import { IconWithText } from './components/IconWithText.tsx';
import { ConfigIcon } from './assets/icon/ConfigIcon.tsx';
import { HelpIcon } from './assets/icon/HelpIcon.tsx';
import { UserProfileIcon } from './assets/icon/UserProfileIcon.tsx';
import { QuickAccessIcon } from './assets/icon/QuickAccessIcon.tsx';
import { MediaMembersList } from './pages/Media/MemberManagement/MediaMembersList.tsx';
import { Faq } from './pages/Help/Faq.tsx';
import { MediaInvitationsList } from './pages/Media/InvitationManagement/MediaInvitationsList.tsx';

interface ProtectedProperties {
  requiredRoles?: string[];
  children: React.ReactElement;
}

// eslint-disable-next-line react-refresh/only-export-components
export const generalPath = '/';
const homePath = 'home';

const authPath = 'auth';
const loginPath = 'login';
const signupPath = 'signup';
const successPath = 'success';
const tacPath = 'tac';
const faqPath = 'faq';
const passwordResetPath = 'password-reset';
const passwordResetConfirmPath = 'password-reset/:token';

const teamsPath = 'teams';
const infoPath = 'info';
const sharedPath = 'shared';

const configurationPath = 'configuration';
const userPath = 'user';
const organizationPath = 'organization';
const membersManagementPath = 'members-management';
const invitationsManagementPath = 'invitations-management';

const configurePath = 'configure';
const welcomePath = 'welcome';

const toolsPath = 'tools';
const recorderPath = 'recorder';
const editorPath = 'editor';
const mySpacePath = 'my-space';

// eslint-disable-next-line react-refresh/only-export-components
export const teamInfoFullPath = (teamId: number): string => {
  return generalPath + teamsPath + '/' + teamId + '/' + infoPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const teamSharedFilesFullPath = (teamId: number): string => {
  return generalPath + teamsPath + '/' + teamId + '/' + sharedPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const teamChatFullPath = (teamId: number): string => {
  return generalPath + teamsPath + '/' + teamId;
};

// eslint-disable-next-line react-refresh/only-export-components
export const welcomeFullPath = (): string => {
  return generalPath + userPath + '/' + welcomePath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const configurationFullPath = (): string => {
  return generalPath + userPath + '/' + configurePath;
};

const userConfigurationFullPath = (): string => {
  return generalPath + configurationPath + '/' + userPath;
};

const organizationConfigurationFullPath = (): string => {
  return generalPath + configurationPath + '/' + organizationPath;
};

const membersManagementConfigurationFullPath = (): string => {
  return generalPath + configurationPath + '/' + membersManagementPath;
};

const invitationsManagementConfigurationFullPath = (): string => {
  return generalPath + configurationPath + '/' + invitationsManagementPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const loginFullPath = (): string => {
  return generalPath + authPath + '/' + loginPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const signupFullPath = (): string => {
  return generalPath + authPath + '/' + signupPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const successFullPath = (): string => {
  return generalPath + authPath + '/' + successPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const teamsFullPath = (): string => {
  return generalPath + teamsPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const homeFullPath = (): string => {
  return generalPath + homePath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const signupTACFullPath = (): string => {
  return signupFullPath() + '/' + tacPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const passwordResetFullPath = (): string => {
  return generalPath + authPath + '/' + passwordResetPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const toolsFullPath = (): string => {
  return generalPath + toolsPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const recorderFullPath = (): string => {
  return generalPath + toolsPath + '/' + recorderPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const editorFullPath = (): string => {
  return generalPath + toolsPath + '/' + editorPath;
};

// eslint-disable-next-line react-refresh/only-export-components
export const mySpaceFullPath = (teamId: number): string => {
  return generalPath + mySpacePath + '/' + teamId;
};

const routesToExcludeIfAuthenticated: string[] = [authPath];
const routesToExcludeIfActionIsCompleted: string[] = [
  welcomePath,
  configurePath,
];

const ProtectedRoute = ({
  children,
  requiredRoles = [],
}: ProtectedProperties): React.ReactElement => {
  const [getComponentToRender, setComponentToRender] =
    useState<React.ReactElement>(<div />);
  const location = useLocation();
  const getUserRoles: string[] = useSelector(getRoles);
  const getLoggedStatus: boolean = useSelector(isLoggedIn);
  const getPreConfigurationIsCompletedFromState: boolean =
    useSelector(isCompleted);
  const getPreConfigurationIsLoadedFromState: boolean = useSelector(
    getIsLoadedCompletely,
  );
  const getRefreshTokenFromStore: string = useSelector(getRefreshToken);
  const getAccessTokenFromStore: string = useSelector(getAccessToken);
  const dispatch: Dispatch<AnyAction> = useDispatch();
  const navigate = useNavigate();

  const routeExcluded = (routes: string[]) =>
    routes.some((route) => location.pathname.includes(route));

  const componentToRender = () => {
    if (getLoggedStatus) {
      verifyAndRefreshToken(
        getRefreshTokenFromStore,
        getAccessTokenFromStore,
        dispatch,
      ).catch(() => {
        logout()(dispatch);
        navigate(loginFullPath(), { replace: true });
      });
    }

    if (
      getPreConfigurationIsCompletedFromState &&
      routeExcluded(routesToExcludeIfActionIsCompleted)
    ) {
      return <Navigate to={homeFullPath()} />;
    }

    if (location.pathname == '/') {
      return <Navigate to={homeFullPath()} />;
    }

    if (getLoggedStatus && routeExcluded(routesToExcludeIfAuthenticated)) {
      return <Navigate to={homeFullPath()} />;
    }

    if (!getLoggedStatus) {
      return routeExcluded(routesToExcludeIfAuthenticated) ? (
        children
      ) : (
        <Navigate to={loginFullPath()} />
      );
    }

    return hasAnyRole(getUserRoles, requiredRoles) ? (
      children
    ) : (
      <Navigate to={teamsFullPath()} />
    );
  };

  useEffect(() => {
    setComponentToRender(componentToRender());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.pathname, getPreConfigurationIsLoadedFromState]);

  return getComponentToRender;
};

const ParentRoute = ({ path }: { path: string }): React.ReactElement => {
  const location = useLocation();
  return location.pathname == path || location.pathname == path + '/' ? (
    <Navigate to={teamsFullPath()} />
  ) : (
    <Outlet />
  );
};

export const Router = createBrowserRouter(
  createRoutesFromElements(
    <Route
      path={generalPath}
      handle={{ title: 'title.main', showCompletely: true, header: Main }}
      element={
        <ProtectedRoute>
          <AppLayout />
        </ProtectedRoute>
      }
    >
      <Route
        handle={{
          title: 'title.main',
          showCompletely: true,
          showNavMenu: true,
          header: Main,
          background: HomeKey,
        }}
        path={'/home'}
        element={<Home />}
      />
      <Route
        path={teamsPath}
        handle={{
          title: 'title.teams.main',
          showNavMenu: true,
          showCompletely: true,
          header: ActionMenus,
          background: TeamListKey,
          isPathTraceable: true,
        }}
      >
        <Route
          index={true}
          handle={{
            title: 'title.teams.main',
            showNavMenu: true,
            showCompletely: true,
            header: ActionMenus,
            background: TeamListKey,
            isPathTraceable: true,
          }}
          element={
            <ProtectedRoute>
              <TeamList />
            </ProtectedRoute>
          }
        />
        <Route
          path={':teamId' + '/' + infoPath}
          handle={{
            title: 'title.teams.info',
            showNavMenu: true,
            showCompletely: true,
            header: ActionMenus,
            background: ActionMenus,
            isPathTraceable: true,
          }}
          element={
            <ProtectedRoute>
              <TeamInfo />
            </ProtectedRoute>
          }
        />
        <Route
          path={':teamId' + '/' + sharedPath}
          handle={{
            title: 'title.teams.shared',
            showNavMenu: true,
            showCompletely: true,
            header: BreadCrumb,
            isPathTraceable: true,
          }}
          element={
            <ProtectedRoute>
              <TeamFiles />
            </ProtectedRoute>
          }
        />
        <Route
          path={':teamId'}
          handle={{
            title: 'title.teams.chat',
            showNavMenu: true,
            showCompletely: true,
            header: ActionMenus,
            background: ActionMenus,
            isPathTraceable: true,
            actionsMenu: [<TeamChatDropdown key={'Action-Menu-Chat'} />],
          }}
          element={
            <ProtectedRoute>
              <TeamChat />
            </ProtectedRoute>
          }
        />
      </Route>
      <Route
        path={mySpacePath}
        handle={{
          title: 'title.teams.main',
          showNavMenu: true,
          showCompletely: true,
          header: Main,
          background: ActionMenus,
        }}
      >
        <Route
          path={':teamId'}
          handle={{
            title: 'title.teams.chat',
            showNavMenu: true,
            showCompletely: true,
            header: ActionMenus,
            background: ActionMenus,
            actionsMenu: [<TeamChatDropdown key={'Action-Menu-Chat'} />],
          }}
          element={
            <ProtectedRoute>
              <TeamChat key={'Personal-Work-Space'} />
            </ProtectedRoute>
          }
        />
      </Route>
      <Route
        path={authPath}
        handle={{
          title: 'title.auth.main',
          showCompletely: false,
          showNavMenu: false,
        }}
        element={<ParentRoute path={generalPath + authPath} />}
      >
        <Route
          path={loginPath}
          handle={{
            footer: AuthFooterKey,
            title: 'title.auth.login',
            showCompletely: false,
            showNavMenu: false,
          }}
          element={
            <ProtectedRoute>
              <AuthPage flag={LOGIN} children={<AuthContent />} />
            </ProtectedRoute>
          }
        />
        <Route
          path={successPath}
          handle={{
            footer: AuthFooterKey,
            title: 'title.auth.success',
            showCompletely: false,
            showNavMenu: false,
          }}
          element={
            <ProtectedRoute>
              <AuthPage flag={LOGIN} children={<WelcomeSection />} />
            </ProtectedRoute>
          }
        />
        <Route
          path={signupPath}
          handle={{
            footer: AuthFooterKey,
            title: 'title.auth.signup',
            showCompletely: false,
            showNavMenu: false,
          }}
          element={<ParentRoute path={signupFullPath()} />}
        >
          <Route
            path={':token'}
            handle={{
              footer: AuthFooterKey,
              title: 'title.auth.signup',
              showCompletely: false,
              showNavMenu: false,
            }}
            element={
              <ProtectedRoute>
                <AuthPage flag={SIGNUP} children={<AuthContent />} />
              </ProtectedRoute>
            }
          />
          <Route
            path={tacPath}
            handle={{
              title: 'title.use_and_privacy_agreement',
              showCompletely: false,
              showNavMenu: false,
            }}
            element={
              <ProtectedRoute>
                <TermsAndConditions isProtected={true} />
              </ProtectedRoute>
            }
          />
        </Route>
        <Route
          path={passwordResetPath}
          handle={{
            footer: AuthFooterKey,
            title: 'title.auth.password_reset',
            showCompletely: false,
            showNavMenu: false,
          }}
          element={
            <ProtectedRoute>
              <PasswordReset />
            </ProtectedRoute>
          }
        />
        <Route
          path={passwordResetConfirmPath}
          handle={{
            footer: AuthFooterKey,
            title: 'title.auth.password_reset_confirm',
            showCompletely: false,
            showNavMenu: false,
          }}
          element={
            <ProtectedRoute>
              <PasswordResetConfirm />
            </ProtectedRoute>
          }
        />
      </Route>
      <Route path={userPath} element={<ParentRoute path={userPath} />}>
        <Route
          path={welcomePath}
          handle={{
            title: 'title.user.welcome_to_preconfiguration',
            showCompletely: false,
            showNavMenu: false,
          }}
          element={
            <ProtectedRoute>
              <AccessToInitialConfiguration />
            </ProtectedRoute>
          }
        />
        <Route
          path={configurePath}
          handle={{
            title: 'title.user.welcome_to_preconfiguration',
            showCompletely: false,
            showNavMenu: false,
          }}
          element={
            <ProtectedRoute>
              <InitialConfiguration />
            </ProtectedRoute>
          }
        />
      </Route>
      <Route
        path={tacPath}
        element={
          <ProtectedRoute>
            <TermsAndConditions />
          </ProtectedRoute>
        }
        handle={{
          title: 'title.faq',
          showCompletely: true,
          showNavMenu: true,
          header: Main,
        }}
      />
      <Route
        path={faqPath}
        element={
          <ProtectedRoute>
            <Faq />
          </ProtectedRoute>
        }
        handle={{
          title: 'title.use_and_privacy_agreement',
          showCompletely: true,
          showNavMenu: true,
          header: Main,
        }}
      />
      <Route
        path={configurationPath}
        element={<ParentRoute path={configurationPath} />}
      >
        <Route
          path={userPath}
          handle={{
            title: 'title.configuration.user',
            showCompletely: true,
            showNavMenu: true,
            header: Main,
          }}
          element={
            <ProtectedRoute>
              <UserProfileForm className={'configuration-form'} />
            </ProtectedRoute>
          }
        />
        <Route
          path={organizationPath}
          handle={{
            title: 'title.configuration.organization',
            showCompletely: true,
            showNavMenu: true,
            header: Main,
          }}
          element={
            <ProtectedRoute>
              <OrganizationDetailsForm className={'configuration-form'} />
            </ProtectedRoute>
          }
        />
        <Route
          path={membersManagementPath}
          handle={{
            title: 'title.configuration.members_management',
            showCompletely: true,
            showNavMenu: true,
            header: Main,
          }}
          element={
            <ProtectedRoute>
              <MediaMembersList />
            </ProtectedRoute>
          }
        />
        <Route
          path={invitationsManagementPath}
          handle={{
            title: 'title.configuration.members_management',
            showCompletely: true,
            showNavMenu: true,
            header: Main,
          }}
          element={
            <ProtectedRoute>
              <MediaInvitationsList />
            </ProtectedRoute>
          }
        />
      </Route>
      <Route
        path={toolsPath}
        handle={{
          title: 'title.tools.main',
          showNavMenu: true,
          showCompletely: true,
          header: ActionMenus,
          background: Tools,
        }}
      >
        <Route
          index={true}
          handle={{
            title: 'title.tools.main',
            showNavMenu: true,
            showCompletely: true,
            header: ActionMenus,
            background: Tools,
          }}
          element={
            <ProtectedRoute>
              <AccessTools />
            </ProtectedRoute>
          }
        />
        <Route
          path={recorderPath}
          handle={{
            title: 'title.tools.recorder',
            showNavMenu: false,
            showCompletely: false,
            header: ActionMenus,
            background: Tools,
            actionsMenu: [<RecorderDropdown key={'Action-Menu-Recorder'} />],
            contextProps: { emitter: EventEmitter() },
          }}
          element={
            <ProtectedRoute>
              <Recorder />
            </ProtectedRoute>
          }
        />
        <Route
          path={editorPath}
          handle={{
            title: 'title.tools.editor',
            showNavMenu: false,
            showCompletely: false,
            header: ActionMenus,
            background: Tools,
            actionsMenu: [<EditorDropdown key={'Action-Menu-Editor'} />],
            contextProps: { emitter: EventEmitter() },
          }}
          element={
            <ProtectedRoute>
              <Editor />
            </ProtectedRoute>
          }
        />
      </Route>
      <Route
        handle={{
          title: 'title.notfound',
          showCompletely: false,
          showNavMenu: false,
        }}
        path={'*'}
        element={<NotFound />}
      />
    </Route>,
  ),
);

export const Navigation: FunctionComponent = () => {
  const [activeGroup, setActiveGroup] = useState<string>('');
  const [activeItem, setActiveItem] = useState<string>('');
  const token: string = useSelector(getAccessToken);
  const getUserRoles: string[] = useSelector(getRoles);
  const dispatch: Dispatch<AnyAction> = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();

  const onSelect = (selectedItem: {
    groupId: string | number;
    itemId: string | number;
    to: string;
    event: FormEvent<HTMLInputElement>;
  }): void => {
    setActiveGroup(selectedItem.groupId as string);
    setActiveItem(selectedItem.itemId as string);
  };

  const userProfileGroupId = 'user_profile_group';
  const userProfileItemId = 'user_profile_item';
  const mediaProfileGroupId = 'media_profile_group';
  const mediaProfileItemId = 'media_profile_item';
  const mediaMembersManagementItemId = 'media_members_management_item';
  const mediaInvitationsMembersItemId = 'media_invitations_management_item';
  const helpGroupId = 'help_group';
  const helpFaqItemId = 'help_faq_item';
  const helpTacItemId = 'help_tac_item';
  const quickAccessItemId = 'quick_access_item';
  const emergencyItemId = 'emergency_item';
  const logoutItemId = 'logout_item';

  return (
    <Nav className={'sidebar-nav'} onSelect={onSelect}>
      <NavList>
        <NavExpandable
          groupId={userProfileGroupId}
          isActive={activeGroup === userProfileGroupId}
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          title={
            <IconWithText
              icon={<UserProfileIcon />}
              text={t('route.sidebar.user_account.main')}
            />
          }
        >
          <NavItem
            itemId={userProfileItemId}
            isActive={activeItem === userProfileItemId}
            children={
              <NavLink
                children={t('route.sidebar.user_account.user_profile')}
                to={userConfigurationFullPath()}
              />
            }
          />
        </NavExpandable>
        <NavExpandable
          groupId={mediaProfileGroupId}
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          title={
            <IconWithText
              icon={<ConfigIcon />}
              text={t('route.sidebar.admin_configuration.main')}
            />
          }
          isActive={activeGroup === mediaProfileGroupId}
          hidden={!isAdministrator(getUserRoles)}
        >
          <NavItem
            itemId={mediaProfileItemId}
            isActive={activeItem === mediaProfileItemId}
            children={
              <NavLink
                children={t('route.sidebar.admin_configuration.media_profile')}
                to={organizationConfigurationFullPath()}
              />
            }
          />
          <NavItem
            itemId={mediaMembersManagementItemId}
            isActive={activeItem === mediaMembersManagementItemId}
            children={
              <NavLink
                children={t(
                  'route.sidebar.admin_configuration.members_management',
                )}
                to={membersManagementConfigurationFullPath()}
              />
            }
          />
          <NavItem
            itemId={mediaInvitationsMembersItemId}
            isActive={activeItem === mediaInvitationsMembersItemId}
            children={
              <NavLink
                children={t(
                  'route.sidebar.admin_configuration.invitations_management',
                )}
                to={invitationsManagementConfigurationFullPath()}
              />
            }
          />
        </NavExpandable>
        <NavExpandable
          groupId={helpGroupId}
          isActive={activeGroup === helpGroupId}
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          title={
            <IconWithText
              icon={<HelpIcon />}
              text={t('route.sidebar.help.main')}
            />
          }
        >
          <NavItem
            itemId={helpFaqItemId}
            isActive={activeItem === helpFaqItemId}
            children={
              <NavLink children={t('route.sidebar.help.faqs')} to={faqPath} />
            }
          />
          <NavItem
            itemId={helpTacItemId}
            isActive={activeItem === helpTacItemId}
            children={
              <NavLink
                children={t('route.sidebar.help.use_and_privacy_agreement')}
                to={tacPath}
              />
            }
          />
        </NavExpandable>
        <NavItem
          //FIXME To activate the nav item again, you must remove the class and its color.
          className={'pf-c-button pf-m-plain pf-m-disabled'}
          itemId={quickAccessItemId}
          isActive={activeItem === quickAccessItemId}
          children={
            <NavLink
              style={{ color: 'gray' }}
              children={
                <IconWithText
                  icon={<QuickAccessIcon />}
                  text={t('route.sidebar.create_quick_access')}
                />
              }
              to={generalPath}
            />
          }
        />
      </NavList>
      <NavList>
        <NavItem
          //FIXME To activate the nav item again, you must remove the class and its color.
          className={'pf-c-button pf-m-plain pf-m-disabled'}
          id={'emergency_button'}
          itemId={emergencyItemId}
          isActive={activeItem === emergencyItemId}
          children={
            <NavLink
              style={{ color: 'gray' }}
              className={'emergency-link'}
              children={t('route.sidebar.emergency')}
              to={generalPath}
            />
          }
        />
        <NavItem
          id={'logout_button'}
          itemId={logoutItemId}
          isActive={activeItem === logoutItemId}
          onClick={() =>
            AuthService.logout(token).finally(() => {
              logout()(dispatch);
              navigate(loginFullPath(), { replace: true });
              addAlert('success', t('logout.alert.success'))(dispatch);
            })
          }
          children={t('route.sidebar.logout')}
        />
      </NavList>
    </Nav>
  );
};

export const MainNavigationMenu: React.FunctionComponent<{
  variant:
    | 'default'
    | 'horizontal'
    | 'tertiary'
    | 'horizontal-subnav'
    | undefined;
  className: string;
}> = (props) => {
  const [getMySpaceId, setMySpaceId] = useState<number>(0);
  const [isKeyboardVisible, setKeyboardVisible] = useState(true);
  const userToken = useSelector(getAccessToken);
  const location = useLocation();

  useEffect((): void => {
    TeamService.getTeams(userToken).then((response): void => {
      const personalWorkspace = response.data
        .filter((team) => {
          return team.is_personal_workspace;
        })
        .pop();

      if (personalWorkspace) {
        setMySpaceId(personalWorkspace.id);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.pathname]);

  const { t } = useTranslation();

  useEffect(() => {
    const handleKeyboardShow = () => {
      setKeyboardVisible(true);
    };

    const handleKeyboardHide = () => {
      setKeyboardVisible(false);
    };

    window.addEventListener('keyboardDidShow', handleKeyboardShow);
    window.addEventListener('keyboardDidHide', handleKeyboardHide);

    return () => {
      window.removeEventListener('keyboardDidShow', handleKeyboardShow);
      window.removeEventListener('keyboardDidHide', handleKeyboardHide);
    };
  }, []);

  const navItemList: React.ReactElement[] = [
    <NavItem key={'home-nav-item'} id={'home-nav-item'}>
      <NavLink to={homeFullPath()}>
        <ColmenaIcon className={'horizontal-menu-icon'} />
        <Text className={'horizontal-menu-text'}>
          {t('route.main_menu.home')}
        </Text>
      </NavLink>
    </NavItem>,
    <NavItem key={'teams-nav-item'} id={'teams-nav-item'}>
      <NavLink to={teamsFullPath()}>
        <ChatIcon className={'horizontal-menu-icon'} />
        <Text className={'horizontal-menu-text'}>
          {t('route.main_menu.teams')}
        </Text>
      </NavLink>
    </NavItem>,
    <NavItem key={'tools-nav-item'} id={'tools-nav-item'}>
      <NavLink to={toolsFullPath()}>
        <ToolsIcon />
        <Text id={'tools-text'} className={'horizontal-menu-text'}>
          {t('route.main_menu.tools')}
        </Text>
      </NavLink>
    </NavItem>,
    <NavItem key={'my-space-nav-item'} id={'my-space-nav-item'}>
      <NavLink to={mySpaceFullPath(getMySpaceId)}>
        <UserIcon className={'horizontal-menu-icon'} />
        <Text className={'horizontal-menu-text'}>
          {t('route.main_menu.my_space')}
        </Text>
      </NavLink>
    </NavItem>,
    <NavItem
      //FIXME To activate the nav item again, you must remove the class and its color.
      className={'pf-c-button pf-m-plain pf-m-disabled'}
      key={'search-nav-item'}
      id={'search-nav-item'}
      style={{ color: 'gray' }}
    >
      <SearchIcon className={'horizontal-menu-icon'} />
      <Text className={'horizontal-menu-text'}>
        {t('route.main_menu.search')}
      </Text>
    </NavItem>,
  ];

  if (props.variant !== 'horizontal') {
    const indexOfSpecialNavItem = navItemList.findIndex(
      (element: React.ReactElement): boolean =>
        element.props.id === 'tools-nav-item',
    );
    if (indexOfSpecialNavItem !== -1) {
      const removedElement: React.ReactElement = navItemList.splice(
        indexOfSpecialNavItem,
        1,
      )[0];
      navItemList.unshift(removedElement);
    }
  }

  return isKeyboardVisible ? (
    <Nav
      className={props.className}
      variant={props.variant}
      id={'nav-menu'}
      children={<NavList id={'menu-nav-item-list'} children={navItemList} />}
    />
  ) : (
    <Fragment />
  );
};
