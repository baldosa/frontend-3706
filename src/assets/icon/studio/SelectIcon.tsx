import * as React from 'react';

export const SelectIcon = ({
  fill = 'white',
  height = '20',
  style = {},
  width = '20',
}: {
  fill?: string;
  height?: string;
  outline?: boolean;
  style?: object;
  width?: string;
}): React.ReactElement => {
  return (
    <svg
      width={width}
      style={style}
      height={height}
      viewBox="0 0 54 54"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <ellipse
        cx="13.7438"
        cy="12.2729"
        rx="2.24188"
        ry="2.45455"
        fill={fill}
      />
      <ellipse
        cx="32.4587"
        cy="12.2729"
        rx="2.24188"
        ry="2.45455"
        fill={fill}
      />
      <ellipse
        cx="41.0363"
        cy="12.2729"
        rx="2.24188"
        ry="2.45455"
        fill={fill}
      />
      <ellipse
        cx="13.7438"
        cy="51.5454"
        rx="2.24188"
        ry="2.45455"
        fill={fill}
      />
      <ellipse
        cx="23.1013"
        cy="51.5454"
        rx="2.24188"
        ry="2.45455"
        fill={fill}
      />
      <ellipse
        cx="32.4587"
        cy="51.5454"
        rx="2.24188"
        ry="2.45455"
        fill={fill}
      />
      <ellipse
        cx="41.0363"
        cy="51.5454"
        rx="2.24188"
        ry="2.45455"
        fill={fill}
      />
      <ellipse
        cx="23.1013"
        cy="12.2729"
        rx="2.24188"
        ry="2.45455"
        fill={fill}
      />
      <path
        d="M4.73098 6.97412C4.53622 7.31019 4.04938 7.30555 3.86106 6.96582L0.411424 0.74256C0.226695 0.409303 0.467701 0.000153383 0.848733 0.000153346L7.90481 0.000153148C8.29007 0.000153111 8.53059 0.417521 8.33742 0.750854L4.73098 6.97412Z"
        fill={fill}
      />
      <path
        d="M49.9585 6.97412C49.7638 7.31019 49.2769 7.30555 49.0886 6.96582L45.639 0.74256C45.4542 0.409303 45.6952 0.000153383 46.0763 0.000153346L53.1324 0.000153148C53.5176 0.000153111 53.7581 0.417521 53.565 0.750854L49.9585 6.97412Z"
        fill={fill}
      />
      <path
        d="M49.7114 11.9526L49.7114 51.8657"
        stroke={fill}
        strokeWidth="4"
        strokeLinecap="round"
      />
      <path
        d="M4.48389 11.7393L4.48389 51.8657"
        stroke={fill}
        strokeWidth="4"
        strokeLinecap="round"
      />
    </svg>
  );
};
