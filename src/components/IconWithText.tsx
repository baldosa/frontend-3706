import { FunctionComponent, ReactElement } from 'react';

export const IconWithText: FunctionComponent<{
  icon: ReactElement;
  text: string;
}> = (props) => {
  return (
    <span style={{ display: 'inline-flex', alignItems: 'center' }}>
      {props.icon}
      <span
        style={{
          paddingLeft: '10px',
          whiteSpace: 'nowrap',
          fontSize: '15px',
        }}
        children={props.text}
      />
    </span>
  );
};
