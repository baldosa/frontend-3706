import {
  FormGroup,
  Select,
  SelectOption,
  SelectOptionObject,
} from '@patternfly/react-core';
import React, { useEffect, useState } from 'react';
import { ExtendedHelperText } from './ExtendedHelperText.tsx';

export interface ExtendedSelect {
  id: string;
  origin: string;
  fieldNames: string[];
  label: string;
  placeholder: string;
  helperText: string;
  isRequired: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  setValue: (value: any) => void;
  getValue: { key: number | string; value: string } | null;
  isValidated: (value: boolean) => void;
  options: { key: number | string; value: string }[];
}

export const ExtendedSelect: React.FunctionComponent<ExtendedSelect> = (
  props: ExtendedSelect,
) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [isDisabled, setIsDisabled] = React.useState(false);
  const [getSelectedValue, setSelectedValue] = useState<
    SelectOptionObject | undefined
  >(undefined);
  const [getOptions, setOptions] = useState<React.ReactElement[]>([]);

  useEffect(() => {
    if (props.options.length > 0) {
      setOptions(options());
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.options]);

  useEffect(() => {
    if (props.getValue) {
      setSelectedValue(props.getValue.value);
      props.isValidated(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.getValue]);

  const onToggle = (isOpen: boolean) => {
    setIsOpen(isOpen);
  };

  const options = () => {
    return props.options.map((item) => (
      <SelectOption id={`selector_${item.key}`} key={item.key} value={item}>
        {item.value}
      </SelectOption>
    ));
  };

  const selectValue = (
    _event: React.MouseEvent<Element, MouseEvent> | React.ChangeEvent,
    value: SelectOptionObject,
  ): void => {
    setSelectedValue(value);
    props.setValue(value);
    setIsOpen(false);
    props.isValidated(true);
  };

  return (
    <FormGroup label={props.label} isRequired={props.isRequired}>
      <Select
        style={{backgroundColor: '#f8fafc'}}
        toggleId={props.id}
        direction={'down'}
        isOpen={isOpen}
        isDisabled={isDisabled}
        onSelect={selectValue}
        placeholderText={props.placeholder}
        onToggle={onToggle}
        selections={getSelectedValue}
        children={getOptions}
      />
      <ExtendedHelperText
        origin={props.origin}
        fieldNames={props.fieldNames}
        validationRules={[]}
      />
    </FormGroup>
  );
};
