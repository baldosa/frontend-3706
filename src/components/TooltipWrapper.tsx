import { Tooltip } from '@patternfly/react-core';

type Props = {
  reference: string;
  title: string;
  maxWidth?: string;
};

export default function TooltipWrapper({
  reference,
  title,
  maxWidth = '18.75rem',
}: Props) {
  return (
    <Tooltip
      id={`${reference}-tooltip`}
      className="record-button-tooltip"
      content={
        <div
          style={{
            color: 'black',
            padding: '0.3rem',
          }}
        >
          {title}
        </div>
      }
      distance={25}
      maxWidth={maxWidth}
      isVisible={true}
      zIndex={5}
      reference={() => document.getElementById(reference) as HTMLButtonElement}
    />
  );
}
