import React, { useState } from 'react';
import { Button } from '@patternfly/react-core';
import EyeIcon from '@patternfly/react-icons/dist/esm/icons/eye-icon';
import EyeSlashIcon from '@patternfly/react-icons/dist/esm/icons/eye-slash-icon';
import { ExtendedTextInput } from './ExtendedTextInput.tsx';

export interface PasswordInput extends ExtendedTextInput {
  className?: string;
}

export const PasswordInput: React.FunctionComponent<PasswordInput> = (
  props: PasswordInput,
) => {
  const [isVisible, setVisibility] = useState(false);
  const inputType = isVisible ? 'text' : props.type;
  const toggleVisibility = (value: boolean) => setVisibility(!value);

  const onKeyUp = ({ code: code }: { code: string }) => {
    if (['Space', 'Enter', 'NumpadEnter'].includes(code)) {
      toggleVisibility(isVisible);
    }
  };

  const labelIcon = (
    <Button
      id={'hide-password-button'}
      variant="control"
      style={{ display: 'flex', alignItems: 'center' }}
      onClick={() => {
        setVisibility(!isVisible);
      }}
      tabIndex={0}
      onKeyUp={onKeyUp}
    >
      {isVisible ? <EyeIcon style={{}} /> : <EyeSlashIcon style={{}} />}
    </Button>
  );

  return (
    <ExtendedTextInput
      id={'password_text_input'}
      className="password-input"
      origin={props.origin}
      label={props.label}
      labelIcon={labelIcon}
      isRequired={true}
      type={inputType}
      getValue={props.getValue}
      setValue={props.setValue}
      placeholder={props.placeholder}
      helperText={props.helperText}
      rules={props.rules}
      fieldNames={props.fieldNames}
    />
  );
};
