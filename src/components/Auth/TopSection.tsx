import React, { FunctionComponent, useEffect, useState } from 'react';
import {
  Select,
  SelectOption,
  SelectOptionObject,
} from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { useAuthContext } from '../../pages/Auth/AuthPage.tsx';

export const TopSection: FunctionComponent<{ title?: string }> = (props) => {
  const [isHeaderUtilsOpen, setIsHeaderUtilsOpen] = useState<boolean>(false);
  const [selectedHeaderUtils, setSelectedHeaderUtils] = useState<
    string | SelectOptionObject
  >('');
  const languages: Map<string, string> = new Map();
  const { t, i18n } = useTranslation();
  const { profile_properties } = useAuthContext();

  languages.set('English', 'en');
  languages.set('French', 'fr');
  languages.set('Portuguese', 'pt_BR');
  languages.set('Spanish', 'es');

  useEffect(() => {
    const languageFromStorage: string | null = localStorage.getItem('language');
    let language: string = languageFromStorage
      ? languageFromStorage
      : navigator.language;
    language = Array.from(languages.values()).includes(language)
      ? language
      : 'en';
    setSelectedHeaderUtils(language);
    changeLanguage(language);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const changeLanguage = (language: string) => {
    i18n
      .changeLanguage(language)
      .then(() => localStorage.setItem('language', language))
      .catch((error) => console.error(error));
  };

  const headerUtilsOptions: JSX.Element[] = [
    <SelectOption key={0} value={languages.get('English')}>
      {t('languages.english')}
    </SelectOption>,
    <SelectOption key={1} value={languages.get('French')}>
      {t('languages.french')}
    </SelectOption>,
    <SelectOption key={2} value={languages.get('Portuguese')}>
      {t('languages.portuguese')}
    </SelectOption>,
    <SelectOption key={3} value={languages.get('Spanish')}>
      {t('languages.spanish')}
    </SelectOption>,
  ];

  const onHeaderUtilsToggle = (isExpanded: boolean) => {
    setIsHeaderUtilsOpen(isExpanded);
  };

  const onHeaderUtilsSelect = (
    _event: React.MouseEvent<Element, MouseEvent> | React.ChangeEvent,
    value: string | SelectOptionObject,
  ): void => {
    setSelectedHeaderUtils(value);
    changeLanguage(value.toString());
    setIsHeaderUtilsOpen(false);
  };

  return (
    <div id={'topSection'} className={'top-section'}>
      <Select
        className={'language-selector'}
        aria-label="Select Language"
        onToggle={onHeaderUtilsToggle}
        onSelect={onHeaderUtilsSelect}
        selections={selectedHeaderUtils}
        isOpen={isHeaderUtilsOpen}
        children={headerUtilsOptions}
      />
      <h1 id={'titleSession'}>
        {props.title ? props.title : profile_properties?.title}
      </h1>
    </div>
  );
};
