import React, { useEffect, useState } from 'react';
import {
  HelperText,
  HelperTextItem,
  Menu,
  MenuContent,
  MenuList,
  MultipleFileUpload,
  MultipleFileUploadMain,
  MultipleFileUploadStatus,
  MultipleFileUploadStatusItem,
} from '@patternfly/react-core';
import { useTranslation } from 'react-i18next';
import { defineIcon } from '../../utils/FileUtils.tsx';

interface ReadFile {
  name: string;
  data?: string;
  loadResult?: 'danger' | 'success';
  loadError?: DOMException;
}

export const UploadFile: React.FunctionComponent<{
  clearStatus: boolean;
  uploadButtonPressed: boolean;
  hasErrors: boolean;
  setUploadedFiles: React.Dispatch<React.SetStateAction<File[]>>;
  setUploadButtonPressed: React.Dispatch<React.SetStateAction<boolean>>;
  acceptedContentTypes: string | undefined;
}> = (props: {
  clearStatus: boolean;
  uploadButtonPressed: boolean;
  hasErrors: boolean;
  setUploadedFiles: React.Dispatch<React.SetStateAction<File[]>>;
  setUploadButtonPressed: React.Dispatch<React.SetStateAction<boolean>>;
  acceptedContentTypes: string | undefined;
}) => {
  const [getCurrentFiles, setCurrentFiles] = useState<File[]>([]);
  const [getFileData, setFileData] = useState<ReadFile[]>([]);
  const [getGeneralStatus, setGeneralStatus] = useState('inProgress');
  const [getHelperText, setHelperText] = useState<string>('');
  const [getHelperTextStatus, setHelperTextStatus] = useState<
    'success' | 'indeterminate' | 'error' | 'default' | 'warning' | undefined
  >('indeterminate');
  const [showStatus, setShowStatus] = React.useState<boolean>(false);
  const { t } = useTranslation();

  useEffect(() => {
    const multipleFileUpload: Element | null = document.querySelector(
      '.pf-c-multiple-file-upload',
    );
    if (multipleFileUpload) {
      const inputFile: Element | null =
        multipleFileUpload.querySelector('input[type="file"]');
      if (inputFile) {
        inputFile.removeAttribute('multiple');
      }
    }
  }, []);

  useEffect(() => {
    if (props.clearStatus) {
      removeAllFiles();
      setShowStatus(false);
    }
  }, [props.clearStatus]);

  useEffect(() => {
    if (props.hasErrors) {
      setHelperTextStatus('error');
      setHelperText(
        t('structure_menu.teams.chat.send-file.status.error').toString(),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.hasErrors]);

  useEffect(() => {
    props.setUploadedFiles(getCurrentFiles);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getCurrentFiles]);

  useEffect(() => {
    if (getFileData.length < getCurrentFiles.length) {
      setGeneralStatus('inProgress');
      setHelperTextStatus('indeterminate');
      setHelperText(
        t(
          'structure_menu.teams.chat.upload-file.status.indeterminate',
        ).toString(),
      );
    } else if (
      getFileData.every(
        (file: ReadFile): boolean => file.loadResult === 'success',
      )
    ) {
      setGeneralStatus('success');
      setHelperTextStatus('success');
      setHelperText(
        t('structure_menu.teams.chat.upload-file.status.success').toString(),
      );
    } else {
      setGeneralStatus('danger');
      setHelperTextStatus('error');
      setHelperText(
        t('structure_menu.teams.chat.upload-file.status.error').toString(),
      );
    }

    if (!showStatus && getCurrentFiles.length > 0) {
      setShowStatus(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getFileData, getCurrentFiles]);

  useEffect(() => {
    if (props.uploadButtonPressed) {
      handleUploadButton();
      props.setUploadButtonPressed(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.uploadButtonPressed]);

  const removeAllFiles = (): void => {
    setCurrentFiles([]);
    setFileData([]);
    setShowStatus(false);
  };

  const removeFiles = (names: string[]): void => {
    setCurrentFiles(
      getCurrentFiles.filter(
        (currentFile: File): boolean =>
          !names.some((name: string): boolean => name === currentFile.name),
      ),
    );
    setFileData(
      getFileData.filter(
        (currentFile: ReadFile): boolean =>
          !names.some((name: string): boolean => name === currentFile.name),
      ),
    );
  };

  const handleReadSuccess = (data: string, file: File): void => {
    setFileData((prevReadFiles: ReadFile[]) => [
      ...prevReadFiles,
      { data, name: file.name, loadResult: 'success' },
    ]);
  };

  const handleReadFail = (error: DOMException, file: File): void => {
    setFileData((prevReadFiles: ReadFile[]) => [
      ...prevReadFiles,
      { loadError: error, name: file.name, loadResult: 'danger' },
    ]);
  };

  const renderHelperText = (uploadedFile: File) => {
    const fileResult: ReadFile | undefined = getFileData.find(
      (file: ReadFile): boolean => file.name === uploadedFile.name,
    );
    if (fileResult?.loadError) {
      return (
        <HelperText isLiveRegion>
          <HelperTextItem
            variant={'error'}
            style={{
              wordWrap: 'break-word',
              overflowWrap: 'break-word',
              whiteSpace: 'pre-line',
            }}
          >
            {t('structure_menu.teams.chat.upload-file.error-message')}
          </HelperTextItem>
        </HelperText>
      );
    }
  };

  const handleFileDrop = (droppedFiles: File[]): void => {
    const currentFileNames: string[] = getCurrentFiles.map(
      (file: File) => file.name,
    );
    const reUploads: File[] = droppedFiles.filter((droppedFile: File) =>
      currentFileNames.includes(droppedFile.name),
    );

    Promise.resolve()
      .then(() => removeFiles(reUploads.map((file: File) => file.name)))
      .then(() => updateCurrentFiles(droppedFiles));
  };

  const updateCurrentFiles = (files: File[]): void => {
    setCurrentFiles((prevFiles) => [...prevFiles, ...files]);
  };

  const successfullyReadFileCount: number =
    getFileData.filter(
      (file: ReadFile): boolean => file.loadResult === 'success',
    ).length / 2;

  const handleUploadButton = () => {
    const elements: NodeList = document.querySelectorAll(
      `[data-ouia-component-id*="OUIA-Generated-Button-secondary"]`,
    );

    if (elements.length > 0) {
      (elements[0] as HTMLElement)?.click();
    }
  };

  return (
    <Menu
      isScrollable
      isPlain={true}
      className={'structure-menu'}
      style={{ height: 'auto', backgroundColor: 'white' }}
    >
      <MenuContent className={'file-section'}>
        <MultipleFileUpload
          onFileDrop={handleFileDrop}
          dropzoneProps={{
            accept: props.acceptedContentTypes,
          }}
        >
          <MultipleFileUploadMain style={{ display: 'none' }} />
          {showStatus && (
            <MultipleFileUploadStatus
              statusToggleText={t(
                'structure_menu.teams.chat.upload-file.count-files',
                {
                  fileIndex: successfullyReadFileCount,
                  totalFiles: getCurrentFiles.length,
                },
              ).toString()}
              statusToggleIcon={getGeneralStatus}
            >
              <Menu isScrollable>
                <MenuContent>
                  <MenuList className={'upload-file-list'}>
                    {getCurrentFiles.map((file: File, index: number) => (
                      <div
                        className={'pf-c-menu__item'}
                        key={`upload-file-item-${index}`}
                        style={{
                          width: 'auto',
                          display: 'flex',
                          marginRight: '10px',
                          flexDirection: 'row',
                          alignItems: 'center',
                        }}
                      >
                        {defineIcon(file.name)}
                        <MultipleFileUploadStatusItem
                          id={`upload-file-${index}`}
                          file={file}
                          key={file.name}
                          onClearClick={() => removeFiles([file.name])}
                          onReadSuccess={handleReadSuccess}
                          onReadFail={handleReadFail}
                          progressHelperText={renderHelperText(file)}
                        />
                      </div>
                    ))}
                  </MenuList>
                </MenuContent>
              </Menu>
              <HelperText style={{ paddingTop: '5px' }}>
                <HelperTextItem
                  variant={getHelperTextStatus}
                  children={getHelperText}
                />
              </HelperText>
            </MultipleFileUploadStatus>
          )}
        </MultipleFileUpload>
      </MenuContent>
    </Menu>
  );
};
