import React from 'react';
import { Button, Icon } from '@patternfly/react-core';
import FaStopIcon from '@patternfly/react-icons/dist/esm/icons/stop-circle-icon';

export const StopButton = ({
  disabled,
  id,
  onClick,
}: {
  audioState?: string;
  disabled?: boolean;
  id?: string;
  onClick: () => void;
}): React.ReactElement => {
  return (
    <Button
      id={id}
      className={'audio-controls-group'}
      data-testid="stop-button"
      {...(!disabled ? { onClick: onClick } : {})}
    >
      <Icon size="xl" className="audio-controls-group-icon">
        <FaStopIcon style={{ fill: disabled ? '#FF0C00' : '#A4C4FF' }} />
      </Icon>
    </Button>
  );
};
