/* eslint-disable @typescript-eslint/no-empty-function */
import { useState, useEffect } from 'react';
import { useWaveformEmitter } from '../../../hooks/useWaveformEmitter';
import { useTranslation } from 'react-i18next';
import SmallModal from '../../../components/SmallModal';
import { Button } from '@patternfly/react-core';
import { ContextProps } from '../../../pages/AppLayout/AppLayout';
import { useOutletContext } from 'react-router-dom';

type Props = {
  isOpen: boolean;
  handleClose: () => void;
  onDiscardRecording?: () => void;
};

export default function DiscardAudioModal({
  isOpen,
  handleClose,
  onDiscardRecording = () => {},
}: Props) {
  const { t } = useTranslation();
  const [isModalOpen, setModalOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const { emitter } = useOutletContext<ContextProps>();

  const { emitterClear } = useWaveformEmitter({
    emitter,
  });

  const handleDiscardRecording = () => {
    setIsLoading(true);
    emitterClear();

    setTimeout(() => {
      setModalOpen(false);
      setIsLoading(false);
      onDiscardRecording && onDiscardRecording();
    }, 1000);
  };

  useEffect(() => {
    setModalOpen(isOpen);
  }, [isOpen]);

  return (
    <SmallModal
      description={String(t('tools.modal_new_audio.description'))}
      isModalOpen={isModalOpen}
      handleClose={handleClose}
      actions={[
        <Button
          key="confirm"
          isDisabled
          variant="primary"
          onClick={() => console.warn('Not implemented yet')}
        >
          {t('tools.modal_new_audio.save_button')}
        </Button>,
        <Button
          key="cancel"
          variant="link"
          isLoading={isLoading}
          onClick={handleDiscardRecording}
        >
          {t('tools.modal_new_audio.discard_button')}
        </Button>,
      ]}
    />
  );
}
