import { useStopwatch } from 'react-timer-hook';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { getActiveRecordingState } from '../../../../redux/reducers/recordings';
import { setRecordingDuration } from '../../../../redux/actions/recordings';
import { PlaylistStatusEnum } from '../../../../enums/index';
import { useDispatch } from 'react-redux';

function format2digits(value: number) {
  return value < 10 ? `0${value.toString()}` : value;
}

export default function Timer() {
  const dispatch = useDispatch();
  const stopWatch = useStopwatch({
    autoStart: false,
  });
  const { totalSeconds, seconds, minutes, hours } = stopWatch;
  const audioState: PlaylistStatusEnum = useSelector(getActiveRecordingState);

  useEffect(() => {
    switch (audioState) {
      case PlaylistStatusEnum.RECORDING:
        stopWatch.start();
        break;
      case PlaylistStatusEnum.STOPPED:
      case PlaylistStatusEnum.PAUSED:
      case PlaylistStatusEnum.RECORDING_PAUSED:
        dispatch(setRecordingDuration(totalSeconds));
        stopWatch.pause();
        break;
      // case PlaylistStatusEnum.IMPORTED_AUDIO: {
      //   const stopwatchOffset = new Date();
      //   stopwatchOffset.setSeconds(0);
      //   stopwatchOffset.setMilliseconds(0);
      //   stopwatchOffset.setSeconds(stopwatchOffset.getSeconds() + recordingDuration);
      //   stopWatch.reset(stopwatchOffset);
      //   break;
      // }
      default:
        stopWatch.pause();
        break;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [audioState]);

  return (
    <div style={{ display: 'flex', justifyContent: 'center' }}>
      <h4
        data-testid="timer-text"
        style={{
          color: 'white',
          fontSize: 32,
          fontWeight: 'bold',
          marginTop: 10,
        }}
      >
        <span>{format2digits(hours)}</span>:
        <span>{format2digits(minutes)}</span>:
        <span>{format2digits(seconds)}</span>
      </h4>
    </div>
  );
}
