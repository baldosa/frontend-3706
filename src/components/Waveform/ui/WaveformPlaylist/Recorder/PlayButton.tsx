import React from 'react';
import { Button } from '@patternfly/react-core';
import FaPlayIcon from '@patternfly/react-icons/dist/esm/icons/play-icon';

export const PlayButton = ({
  onClick,
  disabled,
}: {
  disabled?: boolean;
  onClick: () => void;
}): React.ReactElement => {
  return (
    <Button
      id="secondary-play-button"
      className="audio-controls-group-recorder-pause"
      data-testid="play-button"
      style={{ backgroundColor: '#D4D4D4' }}
      {...(!disabled ? { onClick: onClick } : {})}
    >
      <FaPlayIcon style={{ fill: '#ff271d' }} />
    </Button>
  );
};
