import React, { RefObject, useState } from 'react';
import { Button } from '@patternfly/react-core';
import { MicrophoneIcon } from '../../../../assets/icon/studio/MicrophoneIcon';

export const RecordButton = ({
  id,
  disabled,
  onClick,
}: {
  audioState?: string;
  disabled?: boolean;
  id: string;
  onClick: () => void;
  ref?: RefObject<React.ReactElement>;
}): React.ReactElement => {
  const [isHovered, setHover] = useState(false);

  const handleMouseOver = () => {
    setHover(true);
  };

  const handleMouseOut = () => {
    setHover(false);
  };

  return (
    <Button
      id={id}
      onMouseOver={handleMouseOver}
      onMouseOut={handleMouseOut}
      data-testid="record-button"
      className="audio-controls-record"
      style={{
        display: 'flex',
        justifyContent: 'center',
        borderStyle: 'solid',
        borderWidth: '2px',
        borderColor: disabled ? '#ff0c00' : '#879DDA',
        transform: !disabled
          ? `${isHovered ? 'scale(1.1, 1.1)' : 'scale(1,1)'}`
          : '',
        transition: !disabled ? `${isHovered ? '0.5s' : '0.5s'}` : '',
        backgroundColor: disabled ? 'black' : 'white',
      }}
      {...(!disabled ? { onClick: onClick } : {})}
    >
      <div
        className={`record-button-inactive-outer ${disabled ? 'disabled' : ''}`}
      >
        <div
          className={`record-button-inactive-inner ${
            disabled ? 'disabled' : ''
          }`}
        >
          <MicrophoneIcon
            width={'28'}
            height={'28'}
            outline={!disabled && !isHovered}
            fill={disabled ? '#1e1c33' : '#ff0c00'}
          />
        </div>
      </div>
    </Button>
  );
};
