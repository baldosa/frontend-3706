import React from 'react';
import { Button, Icon } from '@patternfly/react-core';
import FaPauseIcon from '@patternfly/react-icons/dist/esm/icons/pause-circle-icon';

export const PauseButton = ({
  onClick,
  disabled,
}: {
  audioState?: string;
  disabled?: boolean;
  onClick: () => void;
}): React.ReactElement => {
  return (
    <Button
      className={'audio-controls-group'}
      data-testid="pause-button"
      {...(!disabled ? { onClick: onClick } : {})}
    >
      <Icon size="lg" className="audio-controls-group-icon">
        <FaPauseIcon style={{ fill: disabled ? '#FF6300' : '#A4C4FF' }} />
      </Icon>
    </Button>
  );
};
