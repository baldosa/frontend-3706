import { useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  Dropdown,
  DropdownGroup,
  DropdownItem,
  DropdownToggle,
  Form,
  FormGroup,
  TextInput,
  Button,
  List,
  ListItem,
} from '@patternfly/react-core';
import SmallModal from '../../SmallModal';
import ExclamationCircleIcon from '@patternfly/react-icons/dist/esm/icons/exclamation-circle-icon';
import {
  generateRecordingName,
  isValidFilename,
  isAudioEncoderAvailable,
} from '../../../utils/utils';
import { useTranslation } from 'react-i18next';
import { setRecordingName } from '../../../redux/actions/recordings';
import { AudioExportTypesProps } from '../../../types';
import { AudioExportTypesEnum } from '../../../enums';
import { useVerifyScreenOrientation } from '../../../hooks/useVerifyScreenOrientation';

type Props = {
  isModalOpen: boolean;
  handleClose: () => void;
  onFinished: (type: AudioExportTypesProps) => void;
};

export default function ExportAudioModal({
  isModalOpen,
  handleClose,
  onFinished,
}: Props) {
  type validate = 'success' | 'warning' | 'error' | 'default';

  const { t, i18n } = useTranslation();
  const { isPortraitView } = useVerifyScreenOrientation();
  const [title, setTitle] = useState(generateRecordingName(i18n.language));
  const [validated, setValidated] = useState<validate>('default');
  const [errorMessage, setErrorMessage] = useState<string>('');
  const [isModalInfoOpen, setIsModalInfoOpen] = useState(false);
  const [isModalExportLandscapeView, setIsModalExportLandscapeView] =
    useState(false);
  const dispatch = useDispatch();

  const handleTitleChange = (title: string) => {
    setTitle(title);
    if (title === '') {
      setValidated('error');
      setErrorMessage(
        String(t('tools.modal_export_audio.form.messages.field_required')),
      );
    } else if (!isValidFilename(title)) {
      setValidated('error');
      setErrorMessage(
        String(t('tools.modal_export_audio.form.messages.invalid_name')),
      );
    } else {
      setValidated('default');
      setErrorMessage('');
    }
  };

  const [isOpen, setIsOpen] = useState(false);

  const onToggle = (isOpen: boolean) => {
    setIsOpen(isOpen);
  };

  const onSelect = () => {
    setIsOpen(false);
  };

  const handleExportAudio = (type: AudioExportTypesProps) => {
    if (!isAudioEncoderAvailable()) {
      setIsModalInfoOpen(true);
      return;
    }

    if (title) {
      dispatch(setRecordingName(title));
      onFinished(type);
      handleCloseModalInfo();
    }
  };

  const dropdownItems = [
    <DropdownGroup
      label={t('tools.modal_export_audio.dropdown_group_1')}
      key="group 2"
      className="dropdown-group-buttons-export-audio"
    >
      <DropdownItem
        key="wav action"
        component="button"
        onClick={() => handleExportAudio(AudioExportTypesEnum.WAV)}
      >
        {t('tools.modal_export_audio.audio_extensions.wav')}
      </DropdownItem>
      <DropdownItem
        key="opus action"
        component="button"
        onClick={() => handleExportAudio(AudioExportTypesEnum.OPUS)}
      >
        <div className="dropdown-item-title-with-icon">
          <span>{t('tools.modal_export_audio.audio_extensions.opus')}</span>
          {!isAudioEncoderAvailable() && (
            <ExclamationCircleIcon color="orange" />
          )}
        </div>
      </DropdownItem>
    </DropdownGroup>,
    <DropdownGroup
      label={t('tools.modal_export_audio.dropdown_group_2')}
      key="group 3"
    >
      <DropdownItem
        key="export project"
        component="button"
        onClick={() => handleExportAudio(AudioExportTypesEnum.ZIP)}
      >
        {t('tools.modal_export_audio.audio_extensions.zip')}
      </DropdownItem>
    </DropdownGroup>,
  ];

  const landscapeExportItems = (
    <List isPlain isBordered>
      <ListItem onClick={() => handleExportAudio(AudioExportTypesEnum.WAV)}>
        {t('tools.modal_export_audio.audio_extensions.wav')}
      </ListItem>
      <ListItem onClick={() => handleExportAudio(AudioExportTypesEnum.OPUS)}>
        <div className="dropdown-item-title-with-icon">
          <span>{t('tools.modal_export_audio.audio_extensions.opus')}</span>
          {!isAudioEncoderAvailable() && (
            <ExclamationCircleIcon color="orange" />
          )}
        </div>
      </ListItem>
      <ListItem style={{ fontSize: 12, color: 'gray', fontWeight: 'bold' }}>
        {t('tools.modal_export_audio.dropdown_group_2')}
      </ListItem>
      <ListItem onClick={() => handleExportAudio(AudioExportTypesEnum.ZIP)}>
        {t('tools.modal_export_audio.audio_extensions.zip')}
      </ListItem>
    </List>
  );

  const dropdownButton = (
    <Dropdown
      onSelect={onSelect}
      key="dropdown-export-button"
      toggle={
        <DropdownToggle
          isPrimary
          style={{ width: 150 }}
          id="toggle-groups"
          key="toggle-groups"
          isDisabled={errorMessage !== ''}
          onToggle={onToggle}
        >
          {t('tools.modal_export_audio.export_button')}
        </DropdownToggle>
      }
      isOpen={isOpen}
      dropdownItems={dropdownItems}
      isGrouped
    />
  );

  const buttonExportLandscapeMode = (
    <Button
      variant="primary"
      onClick={() => setIsModalExportLandscapeView(true)}
      className="export-landscape-button"
    >
      {t('tools.modal_export_audio.export_button')}
    </Button>
  );

  const handleCloseModalInfo = () => {
    setIsModalInfoOpen(false);
    setIsModalExportLandscapeView(false);
  };

  const handleCloseModalExportButton = () => {
    setIsModalExportLandscapeView(false);
  };

  return (
    <div className="export-audio-modal">
      <SmallModal
        isModalOpen={isModalOpen}
        handleClose={handleClose}
        title={String(t('tools.modal_export_audio.title'))}
        actions={[isPortraitView ? dropdownButton : buttonExportLandscapeMode]}
        content={
          <>
            <SmallModal
              description={String(t('tools.availableOnlyInChromeMessage'))}
              isModalOpen={isModalInfoOpen}
              handleClose={handleCloseModalInfo}
            />
            <SmallModal
              title={String(t('tools.modal_export_audio.dropdown_group_1'))}
              content={landscapeExportItems}
              isModalOpen={isModalExportLandscapeView}
              handleClose={handleCloseModalExportButton}
            />
            <Form>
              <FormGroup
                type="string"
                helperTextInvalid={t(
                  'tools.modal_export_audio.form.messages.field_required',
                )}
                helperTextInvalidIcon={<ExclamationCircleIcon />}
                fieldId="title-1"
                validated={validated}
              >
                <TextInput
                  validated={validated}
                  value={title}
                  id="title-1"
                  aria-describedby="title-1-helper"
                  onChange={handleTitleChange}
                />
              </FormGroup>
            </Form>
          </>
        }
      />
    </div>
  );
}
