import { Modal, ModalVariant } from '@patternfly/react-core';
import React from 'react';

type Props = {
  description?: string;
  actions?: React.ReactNode;
  content?: React.ReactNode;
  title?: string;
  isModalOpen: boolean;
  handleClose: () => void;
  style?: object;
  width?: string | number;
  className?: string
};

export default function SmallModal({
  description = '',
  actions,
  title = '',
  content = <div></div>,
  isModalOpen,
  handleClose,
  style = {},
  width = 'inherit',
  className = ''
}: Props) {
  return (
    <Modal
      variant={ModalVariant.small}
      title={title}
      isOpen={isModalOpen}
      onClose={handleClose}
      actions={actions}
      key="small-modal-example"
      aria-label="small-modal"
      width={width}
      tabIndex={0}
      style={{ ...style }}
      className={className}
    >
      {description}
      {content}
    </Modal>
  );
}
