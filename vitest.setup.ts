import { expect, afterAll, beforeAll } from 'vitest';
import matchers from '@testing-library/jest-dom/matchers';

import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import translation from './public/locales/en/translation.json';

i18n.use(initReactI18next).init({
  resources: { en: { translation } },
  lng: 'en',
  fallbackLng: 'en',
  debug: false,
  interpolation: {
    escapeValue: false,
  },
});

beforeAll(() => {
  global.i18n = i18n;
});
afterAll(() => {
  delete global.i18n;
});

expect.extend(matchers);
